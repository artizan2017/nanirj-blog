<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NnrjBlogComponents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nnrj_blog_components', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->string('content',2000)->nullable();
            $table->integer('blog_id')->default(0);
            $table->integer('comp_seq')->default(0);
            $table->string('identifier',2000)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
