<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NnrjBlog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nnrj_blog', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',2000)->nullable();
            $table->integer('category')->default(0);
            $table->integer('visible')->default(0);
            $table->string('meta_title',2000)->nullable();	
            $table->string('meta_desc',2000)->nullable();		
            $table->string('meta_tags',2000)->nullable();
            $table->string('slug',2000)->nullable();	
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
