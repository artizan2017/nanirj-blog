<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['isAdmin'])->group(function () {
//NANIRJ BLOG
    Route::get('admin/blogs', 'NanirjController@getBlogTbl');
    Route::get('admin/get-add-blog', 'NanirjController@getAddBlog');
    Route::post('admin/get-add-blog', 'NanirjController@saveBlog');
    Route::get('admin/get-append-blog/{blogID}', 'NanirjController@getAppendBlog');
    Route::post('admin/get-append-blog/{blogID}', 'NanirjController@saveAppendBlog');
    Route::get('admin/get-edit-blog/{id}', 'NanirjController@getEditBlog');
    Route::post('admin/get-edit-blog/{id}', 'NanirjController@updateBlog');
    Route::get('admin/get-edit-meta/{id}', 'NanirjController@getEditMeta');
    Route::post('admin/get-edit-meta/{id}', 'NanirjController@updateMeta');
    Route::get('admin/get-edit-comp/{id}', 'NanirjController@getEditComp');
    Route::post('admin/get-edit-comp/{id}', 'NanirjController@updateComp');
    Route::get('admin/get-destroy-blog/{id}', 'NanirjController@destroy');
    Route::get('admin/get-destroy-comp/{id}', 'NanirjController@destroyComp');
    Route::get('admin/get-add-component/{type}/{compIndex}', 'NanirjController@getAddComponent');
    Route::get('admin/get-add-component-v2/{compStr}/{valStr}', 'NanirjController@getAddComponentV2');
    Route::get('admin/get-add-category', 'CategoryController@create');
    Route::post('admin/get-add-category', 'CategoryController@store');
    Route::get('admin/get-edit-category/{id}', 'CategoryController@edit');
    Route::post('admin/get-edit-category/{id}', 'CategoryController@update');
    Route::get('admin/get-destroy-category/{id}', 'CategoryController@destroy');
    Route::get('admin/categories', 'CategoryController@index');
    Route::get('admin/7787489test/{param}', 'NanirjController@testmethod');
});
//FRONT
    Route::get('category/{slug}', 'ViewController@viewCat');
    Route::get('/{slug}', 'ViewController@viewBlog');
    Route::get('/', 'ViewController@viewHome');
    Route::get('admin/login','NanirjController@getAdminLogin');
    Route::post('admin/login','NanirjController@post_login');
    Route::get('admin/logout','NanirjController@admin_logout');
    Route::get('admin/xyzabc123789register','NanirjController@getAdminRegister');
    Route::post('admin/xyzabc123789register','NanirjController@post_register');
    Route::get('admin/xyzabc123789populate','NanirjController@populateDB');

//STATIC FRONT
    Route::get('static/about-us', 'ViewController@viewAboutUs');
    Route::get('static/terms-and-conditions', 'ViewController@viewTerms');
    Route::get('static/contact-us', 'ViewController@viewContactUs');
