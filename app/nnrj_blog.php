<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nnrj_blog extends Model
{
    protected $table = 'nnrj_blog';
    protected $fillable = ['name','category','visible','meta_title','meta_desc','meta_tags','slug'];
}
