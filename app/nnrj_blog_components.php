<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nnrj_blog_components extends Model
{
    protected $table = 'nnrj_blog_components';
    protected $fillable = ['type','content','blog_id','comp_seq'];
}
