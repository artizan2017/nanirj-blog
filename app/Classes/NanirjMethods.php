<?php

namespace App\Classes;
use App\nnrj_blog;
use App\nnrj_blog_components;
use App\nnrj_blog_images;
use App\nnrj_categories;
use Illuminate\Http\Request;



class NanirjMethods {
    public function saveBlog(Request $request) {
        //save blog, insert components 1by1, if its an image then record the image also in the DB 
        $blog = new nnrj_blog();
        $blog->name = $request->input('metatitle');
        $blog->meta_title = $request->input('metatitle');
        $blog->meta_desc = $request->input('metadesc');
        $blog->meta_tags = $request->input('metatags');
        $blog->slug = $this->sluggify($request->input('metatitle'));
        $blog->category = $request->input('category');
        $blog->save();
        $compArr = explode(',',$request->input('compSeq'));
        for($i = 0;$i < sizeof($compArr);$i++) {
            $type = explode('_',$compArr[$i]);
            $blogComp = new nnrj_blog_components();
            $blogComp->type = $type[0];
            $blogComp->content = $this->getContent($request,$compArr[$i],$type[0]);
            $blogComp->blog_id = $blog->id;
            $blogComp->comp_seq = $i + 1;
            $blogComp->save();
            if($type[0] == 'img') {
                $image = $request->file($compArr[$i]);
                $ext = $image->getClientOriginalExtension();
                $imageName = $blog->id."_".$i.".".$ext ;
                $image->move(base_path() . '/public/uploads/blogimgs/', $imageName);

                nnrj_blog_images::create([
                    'name' => $imageName,
                    'type' => 'blogimg',
                    'article_id' => $blogComp->id,
                ]);
            }
        }
    }
    public function appendBlog(Request $request,$blogID) {
        $lastComp = nnrj_blog_components::where('blog_id',$blogID)->orderBy('comp_seq', 'desc')->first();
        $lastIndex = $lastComp->comp_seq;
        $compArr = explode(',',$request->input('compSeq'));
        for($i = 0;$i < (sizeof($compArr));$i++) {
            $type = explode('_',$compArr[$i]);
            $blogComp = new nnrj_blog_components();
            $blogComp->type = $type[0];
            $blogComp->content = $this->getContent($request,$compArr[$i],$type[0]);
            $blogComp->blog_id = $blogID;
            $blogComp->comp_seq = $i + $lastIndex + 1;
            $blogComp->save();
            if($type[0] == 'img') {
                $image = $request->file($compArr[$i]);
                $ext = $image->getClientOriginalExtension();
                $imageName = $blogID."_".$blogComp->id.".".$ext ;
                $image->move(base_path() . '/public/uploads/blogimgs/', $imageName);

                nnrj_blog_images::create([
                    'name' => $imageName,
                    'type' => 'blogimg',
                    'article_id' => $blogComp->id,
                ]);
            }
        }
    }
    function sluggify($str) {
        $slug = mb_strtolower($str,'UTF-8');
        $slug = str_replace(" ","-",$slug);
        return $slug;
    }
    public function updateMeta(Request $request,$blogID) {
        $blog = nnrj_blog::where('id',$blogID)->first();
        $blog->name = $request->input('metatitle');
        $blog->meta_title = $request->input('metatitle');
        $blog->meta_desc = $request->input('metadesc');
        $blog->meta_tags = $request->input('metatags');
        $blog->slug = $this->sluggify($request->input('slug'));
        $blog->save();
        
    }
    public function updateBlog(Request $request,$blogID) {
        $this->clearNoneImgComp($blogID);
        $blog = nnrj_blog::where('id',$blogID)->first();
        $blog->name = $request->input('metatitle');
        $blog->meta_title = $request->input('metatitle');
        $blog->meta_desc = $request->input('metadesc');
        $blog->meta_tags = $request->input('metatags');
        $blog->save();
        $compArr = explode(',',$request->input('compSeq'));
        for($i = 0;$i < sizeof($compArr);$i++) {
            
            $type = explode('_',$compArr[$i]);
                if($type[0] == 'img') {
                    if($request->file($compArr[$i])) {
                        nnrj_blog_components::where('id',$request->input('compID_'.$type[1]))->delete();
                        $blogComp = new nnrj_blog_components();
                        $blogComp->type = $type[0];
                        $blogComp->content = $this->getContent($request,$compArr[$i],$type[0]);
                        $blogComp->blog_id = $blogID;
                        $blogComp->comp_seq = $i + 1;
                        $blogComp->save();

                        $image = $request->file($compArr[$i]);
                        $ext = $image->getClientOriginalExtension();
                        $imageName = $blogID."_".$i.".".$ext ;
                        $image->move(base_path() . '/public/uploads/blogimgs/', $imageName);
        
                        nnrj_blog_images::create([
                            'name' => $imageName,
                            'type' => 'blogimg',
                            'article_id' => $blogComp->id,
                        ]);
                    }
                }else{
                    $type = explode('_',$compArr[$i]);
                    $blogComp = new nnrj_blog_components();
                    $blogComp->type = $type[0];
                    $blogComp->content = $this->getContent($request,$compArr[$i],$type[0]);
                    $blogComp->blog_id = $blogID;
                    $blogComp->comp_seq = $i + 1;
                    $blogComp->save();
                }
            
        }
    }
    public function getAllCats() {
        $cats = nnrj_categories::all();
        foreach($cats as $cat) {
            $cat->featureImg = $this->getCatImg($cat->id);
        }
        return $cats;
    }
    public function getCatImg($id) {
        $img = nnrj_blog_images::where('article_id',$id)->where('type','cat_img')->first();
        return $img->name;
    }
    public function getBlogImg($id) {
        $imgComp = nnrj_blog_components::where('blog_id',$id)->where('type','img')->first();
        $img = nnrj_blog_images::where('article_id',$imgComp->id)->first();
        return $img->name;
    }
    public function getBlog1stPrgph($id) {
        $p = nnrj_blog_components::where('blog_id',$id)->where('type','txtarea')->first();
        return $p->content;
    }
    public function getBlogCat($id) {
        $blog = nnrj_blog::where('id',$id)->first();
        $cat = nnrj_categories::where('id',$blog->category)->first();
        return $cat->name;
    }
    public function getBlogCatSlug($id) {
        $blog = nnrj_blog::where('id',$id)->first();
        $cat = nnrj_categories::where('id',$blog->category)->first();
        return $cat->slug;
    }
    function clearNoneImgComp($blogID) {
        nnrj_blog_components::where('blog_id',$blogID)->where('type','!=','img')->delete();
    }
    
    function getContent($request,$compElem,$type) {
        $content = '';
        if($type != 'img') {
            $content = $request->input($compElem);
        }
        echo ',' .$compElem;
        return $content;
    }
    function getImgCompImg($compID) {
        $img = nnrj_blog_images::where('article_id',$compID)->where('type','blogimg')->first();
        return $img->name;
    }

    public function collectIDsFromFetch($fetch,$currArr,$single = false) {
        if($single) {
            array_push($currArr,$fetch->id);
        }else{
            foreach($fetch as $f) {
                array_push($currArr,$f->id);
            }
        }
        return $currArr;
    }
    public function getUnqRandIDs($catID,$idArr,$qty) {
        for($i = 0;$i < $qty;$i++) {
            $id = $this->getUnqRandID($catID,$idArr);
            array_push($idArr,$id);
        }
        return $idArr;
    }
    public function getUnqRandID($catID,$idArr) {
        $randBlogID = 0;
        while($randBlogID <= 0) {
            $exist = false;
            $blog = nnrj_blog::all()->random(1)->first();
            if($catID > 0) {
                $blog = nnrj_blog::where('category',$catID)->random(1)->first();
            }
            foreach($idArr as $a) {
                if($a == $blog->id) {
                    $exist = true;
                }
            }
            if(!$exist) {
                $randBlogID = $blog->id;
            }
        }
        return $randBlogID;
    }
}