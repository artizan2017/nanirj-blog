<?php

namespace App\Http\Controllers;
use App\Classes\NanirjMethods;
use App\nnrj_blog;
use App\nnrj_blog_components;
use App\nnrj_blog_images;
use App\nnrj_categories;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats = nnrj_categories::all();
        return view('nanirj.cat-tbl',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nanirj.add-cat');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cat = new nnrj_categories();
        $cat->name = $request->input('catname');
        $cat->description = $request->input('catdesc');
        $cat->slug = $request->input('catslug');
        $cat->save();
        $image = $request->file('catimg');
                        $ext = $image->getClientOriginalExtension();
                        $imageName = $cat->id.".".$ext ;
                        $image->move(base_path() . '/public/uploads/catimgs/', $imageName);
        
                        nnrj_blog_images::create([
                            'name' => $imageName,
                            'type' => 'cat_img',
                            'article_id' => $cat->id,
                        ]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = nnrj_categories::where('id',$id)->first();
        return view('nanirj.edit-cat',compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat = nnrj_categories::where('id',$id)->first();
        $cat->name = $request->input('catname');
        $cat->description = $request->input('catdesc');
        $cat->slug = $request->input('catslug');
        $cat->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        nnrj_categories::destroy($id);
        return redirect('admin/categories');
    }
}
