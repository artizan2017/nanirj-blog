<?php

namespace App\Http\Controllers;
use App\Classes\NanirjMethods;
use App\nnrj_blog;
use App\nnrj_blog_components;
use App\nnrj_blog_images;
use App\nnrj_categories;
use App\Mail\contactus;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function viewHome() {
        $methods = new NanirjMethods();
        $cats = $this->getCategories();
        $L1Blog = $this->getRandBlog();
        $L2Blog = $this->getRandBlog();
        $blogCarousel = $this->getRandBlogs(4);
        $trendBlogs = $this->getRandBlogs(12);
        $recentBlogs = $this->getRandBlogs(4);
        $popularBlogs = $this->getRandBlogs(4);
        $commentBlogs = $this->getRandBlogs(4);
        $TabAllBlogs = $this->getRandBlogs(4);
        $TabAllMainBlog = $this->getRandBlog();
        $TabCatBlogsArr = $this->getCatBlogTabs('others');
        $TabCatMainBlogsArr = $this->getCatBlogTabs('main');
        $randCatID = $this->getRandCatID();
        $blackPanelMainBlog = $this->getCatBlog($randCatID);
        $blackPanelOtherBlogs = $this->getRandCatBlogs(4,$randCatID);
        $blockSliderBlogs = $this->getRandBlogs(8);
        $randCatID2 = $this->getRandCatID();
        $whitePanelMainBlog = $this->getCatBlog($randCatID2);
        $whitePanelOtherBlogs1 = $this->getRandCatBlogs(2,$randCatID2);
        $whitePanelOtherBlogs2 = $this->getRandCatBlogs(2,$randCatID2);
        $dontMissBlogs = $this->getRandBlogs(6);
        $bigSliderBlogs = $this->getRandBlogs(4);
        $allCats = $methods->getAllCats();
        return view('welcome',compact('cats','L1Blog','L2Blog','blogCarousel','trendBlogs','recentBlogs','popularBlogs','commentBlogs','TabAllBlogs','TabAllMainBlog','TabCatBlogsArr','TabCatMainBlogsArr'
                                        ,'blackPanelMainBlog','blackPanelOtherBlogs','blockSliderBlogs','whitePanelMainBlog','whitePanelOtherBlogs1','whitePanelOtherBlogs2','allCats'
                                        ,'dontMissBlogs','bigSliderBlogs'));
    }
    public function viewBlog($slug) {
        $method = new NanirjMethods();
        $blog = nnrj_blog::where('slug',$slug)->first();
        $blogCat = nnrj_categories::where('id',$blog->category)->first();
        $blog->catName = $blogCat->name;
        $popularBlogs = $this->getRandBlogs(4);
        $recentBlogs = $this->getRandBlogs(4);
        $commentBlogs = $this->getRandBlogs(4);
        $prev = $this->getRandBlog();
        $next = $this->getRandBlog();
        $relatedBlogs = $this->getRandCatBlogs(3,$blog->category);
        $allCats = $method->getAllCats();
        $cats = $this->getCategories();
        
        $id = $blog->id;
        $comp = nnrj_blog_components::where('blog_id',$blog->id)->orderBy('comp_seq')->get();
        //$prevBlog
        foreach($comp as $c) {
            if($c->type == 'img') {
                $c->img = $method->getImgCompImg($c->id);
            }else{
                $c->img = '';
            }
        }
        return view('blog-post',compact('comp','id','blog','cats','popularBlogs','recentBlogs','recentBlogs','commentBlogs','allCats','prev','next','relatedBlogs'));
    }
    
    public function viewCat($slug) {
        $methods = new NanirjMethods();
        $cats = $this->getCategories();
        $cat = nnrj_categories::where('slug',$slug)->first();
        $id = $cat->id;
        $mainBlog = $this->getCatBlog($id);
        $otherBlogs = $this->getRandCatBlogs(4,$id);
        $allCats = $methods->getAllCats();
        $recentBlogs = $this->getRandBlogs(4);
        $popularBlogs = $this->getRandBlogs(4);
        $commentBlogs = $this->getRandBlogs(4);
        return view('category-blogs',compact('id','cat','cats','mainBlog','otherBlogs','allCats','recentBlogs','popularBlogs','commentBlogs'));
    }
    public function viewAboutUs() {
        $method = new NanirjMethods();
        /*$slug = "";
        $blog = nnrj_blog::where('slug',$slug)->first();
        $blogCat = nnrj_categories::where('id',$blog->category)->first();
        $blog->catName = $blogCat->name;*/
        $popularBlogs = $this->getRandBlogs(4);
        $recentBlogs = $this->getRandBlogs(4);
        $commentBlogs = $this->getRandBlogs(4);
        $prev = $this->getRandBlog();
        $next = $this->getRandBlog();
        //$relatedBlogs = $this->getRandCatBlogs(3,$blog->category);
        $allCats = $method->getAllCats();
        $cats = $this->getCategories();
        
        //$id = $blog->id;
        //$comp = nnrj_blog_components::where('blog_id',$blog->id)->orderBy('comp_seq')->get();
        //$prevBlog
        /*foreach($comp as $c) {
            if($c->type == 'img') {
                $c->img = $method->getImgCompImg($c->id);
            }else{
                $c->img = '';
            }
        }*/
        return view('about-us',compact('cats','popularBlogs','recentBlogs','recentBlogs','commentBlogs','allCats','prev','next'));
    }
    public function viewTerms() {
        $method = new NanirjMethods();
        /*$slug = "";
        $blog = nnrj_blog::where('slug',$slug)->first();
        $blogCat = nnrj_categories::where('id',$blog->category)->first();
        $blog->catName = $blogCat->name;*/
        $popularBlogs = $this->getRandBlogs(4);
        $recentBlogs = $this->getRandBlogs(4);
        $commentBlogs = $this->getRandBlogs(4);
        $prev = $this->getRandBlog();
        $next = $this->getRandBlog();
        //$relatedBlogs = $this->getRandCatBlogs(3,$blog->category);
        $allCats = $method->getAllCats();
        $cats = $this->getCategories();
        
        //$id = $blog->id;
        //$comp = nnrj_blog_components::where('blog_id',$blog->id)->orderBy('comp_seq')->get();
        //$prevBlog
        /*foreach($comp as $c) {
            if($c->type == 'img') {
                $c->img = $method->getImgCompImg($c->id);
            }else{
                $c->img = '';
            }
        }*/
        return view('terms-and-condition',compact('cats','popularBlogs','recentBlogs','recentBlogs','commentBlogs','allCats','prev','next'));
    }
    public function viewContactUs() {
        $method = new NanirjMethods();
        /*$slug = "";
        $blog = nnrj_blog::where('slug',$slug)->first();
        $blogCat = nnrj_categories::where('id',$blog->category)->first();
        $blog->catName = $blogCat->name;*/
        $popularBlogs = $this->getRandBlogs(4);
        $recentBlogs = $this->getRandBlogs(4);
        $commentBlogs = $this->getRandBlogs(4);
        $prev = $this->getRandBlog();
        $next = $this->getRandBlog();
        //$relatedBlogs = $this->getRandCatBlogs(3,$blog->category);
        $allCats = $method->getAllCats();
        $cats = $this->getCategories();
        
        //$id = $blog->id;
        //$comp = nnrj_blog_components::where('blog_id',$blog->id)->orderBy('comp_seq')->get();
        //$prevBlog
        /*foreach($comp as $c) {
            if($c->type == 'img') {
                $c->img = $method->getImgCompImg($c->id);
            }else{
                $c->img = '';
            }
        }*/
        Mail::to('nanirjblog@gmail.com')->send(new contactus());
        return view('contact-us',compact('cats','popularBlogs','recentBlogs','recentBlogs','commentBlogs','allCats','prev','next'));
    }
    function getCatBlogTabs($returnType) {
        $cats = $this->getCategories();
        $retVal = '';
        $catOthersArr = array();
        $catMainArr = array();
        foreach($cats as $cat) {
            $mainBlog = $this->getCatBlog($cat->id);
            $otherBlogs = $this->getRandCatBlogs(4,$cat->id);
            array_push($catMainArr,$mainBlog);
            array_push($catOthersArr,$otherBlogs);
        }
        switch($returnType) {
            case 'main':
                $retVal = $catMainArr;
            break;
            case 'others':
                $retVal = $catOthersArr;
            break;
        }
        return $retVal;
    }
    function getRandCatID() {
        $cats = nnrj_categories::all();
        $catArr = array();
        foreach($cats as $cat) {
            array_push($catArr,$cat->id);
        }
        $randIndex = rand(0, sizeof($catArr) - 1);
        return $catArr[$randIndex];
    }
    function getCatBlog($catID) {
        $methods = new NanirjMethods();
        $blog = nnrj_blog::where('category',$catID)->orderBy('id','DESC')->first();
        $blog->bdate = $this->formatDate($blog->created_at);
        $blog->featureImg = $methods->getBlogImg($blog->id);
        $blog->catName = $methods->getBlogCat($blog->id);
        $blog->previewContent = $methods->getBlog1stPrgph($blog->id);
        $blog->catSlug = $methods->getBlogCatSlug($blog->id);
        return $blog;
    }
    function getRandCatBlogs($qty,$catID) {
        $blogs = nnrj_blog::where('category',$catID)->get();
        if($qty > $blogs->count()) {
            $qty = $blogs->count();
        }
        $methods = new NanirjMethods();
        $blogs = nnrj_blog::where('category',$catID)->orderBy('id','DESC')->paginate($qty);
        foreach($blogs as $blog){
            $blog->bdate = $this->formatDate($blog->created_at);
            $blog->featureImg = $methods->getBlogImg($blog->id);
            $blog->catName = $methods->getBlogCat($blog->id);
            $blog->previewContent = $methods->getBlog1stPrgph($blog->id);
            $blog->catSlug = $methods->getBlogCatSlug($blog->id);
        }
        return $blogs;
    }
    function getRandBlog() {
        $methods = new NanirjMethods();
        $blog = nnrj_blog::all()->random(1)->first();
        $blog->bdate = $this->formatDate($blog->created_at);
        $blog->featureImg = $methods->getBlogImg($blog->id);
        $blog->catName = $methods->getBlogCat($blog->id);
        $blog->catSlug = $methods->getBlogCatSlug($blog->id);
        return $blog;
    }
    function formatDate($date) {
        return date("d M, Y", strtotime($date));
    }
    function getRandBlogs($qty) {
        $blogs = nnrj_blog::all();
        if($qty > $blogs->count()) {
            $qty = $blogs->count();
        }
        $methods = new NanirjMethods();
        $blogs = nnrj_blog::all()->random($qty);
        foreach($blogs as $blog){
            $blog->bdate = $this->formatDate($blog->created_at);
            $blog->featureImg = $methods->getBlogImg($blog->id);
            $blog->catName = $methods->getBlogCat($blog->id);
            $blog->catSlug = $methods->getBlogCatSlug($blog->id);
        }
        return $blogs;
    }
    
    function getCategories() {
        $cats = nnrj_categories::all();
        return $cats;
    }
    function getPopularPosts() {
        $blogs = nnrj_blog::all()->random(4);
        return $blogs;
    }
    
}
