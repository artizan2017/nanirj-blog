<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\NanirjMethods;
use App\nnrj_blog;
use App\nnrj_blog_components;
use App\nnrj_blog_images;
use App\nnrj_categories;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class NanirjController extends Controller
{

    use AuthenticatesUsers;

    public function getEditBlog($id) {
        $blog = nnrj_blog::where('id',$id)->first();
        $method = new NanirjMethods();
        $comp = nnrj_blog_components::where('blog_id',$id)->orderBy('comp_seq')->get();
        foreach($comp as $c) {
            if($c->type == 'img') {
                $c->img = $method->getImgCompImg($c->id);
            }else{
                $c->img = '';
            }
        }
        return view('nanirj.edit-blog')->with('comp',$comp)->with('id',$id)->with('blog',$blog);
    }
    public function getEditMeta($id) {
        $blog = nnrj_blog::where('id',$id)->first();
        
        return view('nanirj.edit-blog-meta')->with('id',$id)->with('blog',$blog);
    }
    public function updateMeta(Request $request,$id) {
        $method = new NanirjMethods();
        $method->updateMeta($request,$id);
        return back();
    }
    public function getEditComp($id) {
        $method = new NanirjMethods();
        $_comp = nnrj_blog_components::where('id',$id)->first();
        if($_comp->type == 'img') {
                $_comp->img = $method->getImgCompImg($_comp->id);
        }else{
                $_comp->img = '';
        }
        $comp = array($_comp);
        return view('nanirj.edit-component',compact('comp','id'));
    }
    public function updateComp(Request $request,$id) {
        $comp = nnrj_blog_components::where('id',$id)->first();
        $comp->content = $request->input($comp->type.'_1');
        $comp->save();
        if($comp->type == 'img') {
                $img = nnrj_blog_images::where('article_id',$id)->first();
                $image = $request->file($comp->type.'_1');
                //$ext = $image->getClientOriginalExtension();
                $imageName = $img->name ;
                $image->move(base_path() . '/public/uploads/blogimgs/', $imageName);
        }
        return back()->with('message','Component successfully updated!');
    }
    public function getAddBlog() {
        $cats = nnrj_categories::all();
        return view('nanirj.add-blog',compact('cats'));
    }
    public function getAppendBlog($blogID) {
        $method = new NanirjMethods();
        $comp = nnrj_blog_components::where('blog_id',$blogID)->orderBy('comp_seq')->get();
        //$prevBlog
        foreach($comp as $c) {
            if($c->type == 'img') {
                $c->img = $method->getImgCompImg($c->id);
            }else{
                $c->img = '';
            }
        }
        return view('nanirj.append-component',compact('blogID','comp'));
    }
    public function saveAppendBlog(Request $request,$blogID) {
        $method = new NanirjMethods();
        $method->appendBlog($request,$blogID);
        return back();
        //return 'asd';
    }
    public function getBlogTbl() {
        $blogs = nnrj_blog::where('visible',1)->orWhere('visible',0)->get();
        return view('nanirj.blog-tbl',compact('blogs'));
    }
    public function saveBlog(Request $request) {
        $method = new NanirjMethods();
        $method->saveBlog($request);
        return back()->with('message','Blog successfully updated!');
        //return 'asd';
    }
    public function updateBlog(Request $request,$id) {
        $method = new NanirjMethods();
        $method->updateBlog($request,$id);
        return back();
    }

    public function destroy($id)
    {
        nnrj_blog::destroy($id);
        return redirect('admin/blogs');
    }
    public function destroyComp($id)
    {
        nnrj_blog_components::destroy($id);
        return back();
    }

    public function getAddComponent($type,$compIndex) {
        return view('nanirj.blog-component',compact('type','compIndex'));
    }
    public function getAddComponentV2($compStr,$valStr) {
        $compArr = explode('|',$compStr);
        $has_ = explode('_',$compArr[0]);
        if(sizeof($has_) > 1) {
            $compStr = '';
            foreach($compArr as $comp) {
                $prefix = explode('_',$comp);
                if($compStr == '') {
                    $compStr .= $prefix[0];
                }else{
                    $compStr .= '|' . $prefix[0];
                }
            }
        }
        return view('nanirj.blog-component-v2',compact('compStr','valStr'));
    }


    public function getAdminLogin() {
        return view('nanirj.login');
    }
    public function post_login(Request $request) {
        $usr = User::where('email',$request->input('email'))->first();
        if($usr){
            if (Hash::check($request->input('password'), $usr->password)) {
                Auth::login($usr);
                return redirect('admin/blogs');
            }
        }
            return redirect('admin/login');
            
        
    }
    public function admin_logout() {
        Auth::logout();
        return redirect('admin/login');
    }
    public function getAdminRegister() {
        return view('nanirj.admin-register');
    }
    public function post_register(Request $request) {
        $user = new User();
        $user->name = 'admin';
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();
        return back();
    }



    //TEST METHOD
    public function testmethod($param) {
        $lastComp = nnrj_blog_components::where('blog_id',50)->orderBy('comp_seq', 'desc')->first();
        $lastIndex = $lastComp->comp_seq;
        return $lastIndex;
    }

    public function populateDB() {
        $cats = array(1,2,3,4);
        $imgs = array('rand1.jpg','rand2.jpg','rand3.jpg','rand4.jpg','rand5.jpg','rand6.jpg');
        $lastImgIndex = sizeof($imgs) - 1;
        
        foreach($cats as $cat) {
            for($i = 0;$i < 12;$i++) {
                $randImgID = rand(0,$lastImgIndex);
                $blog = new nnrj_blog();
                $blog->name = 'يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم';
                $blog->category = $cat;
                $blog->visible = 1;
                $blog->meta_title = 'يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم';
                $blog->meta_desc = 'يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم';
                $blog->meta_tags = 'يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم';
                $blog->slug = 'يتبيرسبايكياتيس-يوندي-أومنيس-أستي-ناتيس-أيررور-سيت-فوليبتاتيم-'.$cat.'-'.$i;
                $blog->save() ;

                $comp = new nnrj_blog_components();
                $comp->type = 'img';
                $comp->content = '';
                $comp->blog_id = $blog->id;
                $comp->comp_seq = 1;
                $comp->save();

                $comp2 = new nnrj_blog_components();
                $comp2->type = 'h1';
                $comp2->content = 'تبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أك';
                $comp2->blog_id = $blog->id;
                $comp2->comp_seq = 2;
                $comp2->save();

                $comp3 = new nnrj_blog_components();
                $comp3->type = 'txtarea';
                $comp3->content = '<p dir="rtl" style="margin: 0px 0px 10px; color: #424242; font-family: tahoma, sans-serif; font-size: 14px; background-color: #ffffff;">ت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم</p>
                <p dir="rtl" style="margin: 0px 0px 10px; color: #424242; font-family: tahoma, sans-serif; font-size: 14px; background-color: #ffffff;">دولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت</p>
                <p dir="rtl" style="margin: 0px 0px 10px; color: #424242; font-family: tahoma, sans-serif; font-size: 14px; background-color: #ffffff;">كياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي</p>
                <p dir="rtl" style="margin: 0px 0px 10px; color: #424242; font-family: tahoma, sans-serif; font-size: 14px; background-color: #ffffff;">فوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجنا</p>';
                $comp3->blog_id = $blog->id;
                $comp3->comp_seq = 3;
                $comp3->save();

                $img = new nnrj_blog_images();
                $img->article_id = $comp->id;
                $img->type = 'blogimg';
                $img->name = $imgs[$randImgID];
                $img->save();
            }
        }

        return view('default-imgs');
    }


    
    
}
