<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nnrj_blog_images extends Model
{
    protected $table = 'nnrj_blog_images';
    protected $fillable = ['article_id','type','name'];
}
