<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nnrj_categories extends Model
{
    protected $table = 'nnrj_categories';
    protected $fillable = ['name','description','slug'];
}
