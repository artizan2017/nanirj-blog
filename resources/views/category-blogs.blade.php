@extends('layouts.front-layout')
@section('content')
<!-- breadcrumb -->
<div class="breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<ol class="breadcrumb" style="justify-content: flex-end;">
						<li><a href="#">{{$cat->name}}</a></li>
						<li>
							<i class="fa fa-angle-left"></i>
							<a href="{{url('/')}}">الرئيسية</a>
								<i class="fa fa-home"></i>
						</li>
						
					</ol>		
				</div>
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<!-- breadcrumb end -->

	<section class="main-content category-layout-1 pt-0">
		<div class="container">
			<div class="row ts-gutter-30">
			<div class="col-lg-4">
					<div class="sidebar">
						@include('partials.social-widget')
						

						<div class="sidebar-widget ads-widget mt-20">
							<div class="ads-image">
								<a href="#">
									<img class="img-fluid" src="{{url('assets/images/banner-image/image2.png')}}" alt="">
								</a>
							</div>
						</div><!-- widget end -->

						@include('partials.tab-blogs')

						@include('partials.category-list')
						
					</div>
				</div>
				<div class="col-lg-8 pull-right-txt">
					<div class="row">
						<div class="col-12">
							<h2 class="block-title">
								<span class="title-angle-shap">{{$cat->name}} </span>
							</h2>
							
							<div class="post-block-style">
								<div class="post-thumb">
									<a href="{{url('/'.$mainBlog->slug)}}">
										<img class="img-fluid" src="{{url('uploads/blogimgs/'.$mainBlog->featureImg)}}" alt="">
									</a>
									<div class="grid-cat">
										<a class="post-cat fashion" href="{{url('/category/'.$mainBlog->catSlug)}}">{{$mainBlog->catName}}</a>
									</div>
								</div>
								
								<div class="post-content">
									<h2 class="post-title title-lg">
										<a href="{{url('/'.$mainBlog->slug)}}">{{$mainBlog->meta_title}}</a>
									</h2>
									<div class="post-meta mb-7">
										<span class="post-date"><i class="fa fa-clock-o"></i> {{$mainBlog->bdate}}</span>
									</div>
									{!!$mainBlog->previewContent!!}
								</div><!-- Post content end -->
							</div>
						</div><!-- col end -->
					</div><!-- row end -->
					<div class="gap-30"></div>
					<div class="row ts-gutter-10">
                        @foreach($otherBlogs as $blog)
						<div class="col-md-6">
							<div class="post-block-style">
								<div class="post-thumb">
									<a href="{{url('/'.$blog->slug)}}">
										<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="">
									</a>
									<div class="grid-cat">
										<a class="post-cat health" href="{{url('/category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
									</div>
								</div>
								
								<div class="post-content">
									<h2 class="post-title title-md">
										<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
									</h2>
									<div class="post-meta mb-7">
										<span class="post-date"><i class="fa fa-clock-o"></i> {{$blog->bdate}}</span>
									</div>
									{!!$blog->meta_desc!!}
								</div><!-- Post content end -->
							</div>
						</div><!-- col end -->
						@endforeach
					</div><!-- row end -->
					<div class="gap-30 d-none d-md-block"></div>
					<div class="row">
						<div class="col-12">
							<!-- <ul class="ts-pagination">
								<li><a href="#">1</a></li>
								<li><a href="#" class="active">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul> -->
                            {{ $otherBlogs->links() }}
						</div>
					</div>
				</div><!-- col-lg-8 -->
				
			</div><!-- sidebar col end -->
		</div><!-- container end -->
    </section><!-- category-layout end -->
    @endsection