@extends('layouts.admin-portal')
@section('content')
        <form id="blog-form" action="{{url('admin/get-add-blog')}}" method="post" onsubmit="event.preventDefault();validateMyForm();" enctype="multipart/form-data">
        <div class="blog-frm-main-container">
            @csrf
            <input type="hidden" id="compSeq" name="compSeq" value="" />
            <input type="hidden" id="valSeq" value="" />
            <input type="hidden" id="rootUrl" value="{{url('/')}}" />
            <input type="hidden" id="component-counter" value="1" />
            <div id="input-container-meta" class="empty-container">
                    <div class="component-container">
                        <div class="main-comp-sec">
                            <b>Meta Title</b><input type="text" name="metatitle" /><br />
                            <b>Meta Description</b><input type="text" name="metadesc" /><br />
                            <b>Meta Tags</b><input type="text" name="metatags" />
                            <b>Category</b>
                                <select name="category">
                                    @foreach($cats as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
            </div>
            <div id="options-container1" class="empty-container">
                <div class="component-container">
                    <div class="add-comp-ctrl-sec">
                        <!--<button onclick="addComponent('h1')" class="ctrl-btn">H1</button>
                        <button onclick="addComponent('txtarea')" class="ctrl-btn">P</button>
                        <button onclick="addComponent('img')" class="ctrl-btn"><i class="fa fa-image" aria-hidden="true"></i></button> -->
                        <button onclick="getBlogComponent('h1',1)" class="ctrl-btn">H1</button>
                        <button onclick="getBlogComponent('h2',1)" class="ctrl-btn">H2</button>
                        <button onclick="getBlogComponent('h3',1)" class="ctrl-btn">H3</button>
                        <button onclick="getBlogComponent('h4',1)" class="ctrl-btn">H4</button>
                        <button onclick="getBlogComponent('txtarea',1)" class="ctrl-btn">P</button>
                        <button onclick="getBlogComponent('img',1)" class="ctrl-btn"><i class="fa fa-image" aria-hidden="true"></i></button>

                    </div>
                </div>
            </div>
            <div id="comp-container" class="empty-container"></div>
            <div id="add-container1" class="empty-container"></div>
    <div class="component-container">
        <div class="add-comp-ctrl-sec">
            <button type="submit" onclick="submitForm()" class="ctrl-btn" style="width: 300px;">Save</button>
        </div>
    </div>
        </div>
        </form>

@endsection



