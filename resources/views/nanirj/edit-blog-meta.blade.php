@extends('layouts.admin-portal')
@section('content')
<form id="blog-form" action="{{url('admin/get-edit-meta/'.$id)}}" method="post" enctype="multipart/form-data">
                    
            <div class="blog-frm-main-container">
                    @csrf                    
                    <div id="input-container-meta" class="empty-container">
                            <div class="component-container">
                                <div class="main-comp-sec">
                                    <b>Meta Title</b><input type="text" name="metatitle" value="{{$blog->meta_title}}" /><br />
                                    <b>Meta Description</b><input type="text" name="metadesc" value="{{$blog->meta_desc}}" /><br />
                                    <b>Meta Tags</b><input type="text" name="metatags" value="{{$blog->meta_tags}}" />
                                    <b>Slug</b><input type="text" name="slug" value="{{$blog->slug}}" />
                                </div>
                            </div>
                    </div>
                    <div class="component-container">
                        <div class="add-comp-ctrl-sec">
                            <button type="submit" class="ctrl-btn" style="width: 300px;">Save</button>
                        </div>
                    </div>
            </div>
</form>
@endsection