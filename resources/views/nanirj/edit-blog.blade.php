@extends('layouts.admin-portal')
@section('content')
            
                    <?php
                        $compIndex = 2;
                        $lastIndex = sizeof($comp) - 1;
                        $compSeq = "";
                        function concatCompSeq($compName,$compSeq) {
                            if($compSeq == '') {
                                $compSeq .= $compName;
                            }else{
                                $compSeq .= ',' . $compName;
                            }
                            return $compSeq;
                        }
                        
                    ?>
                    <form id="blog-form" action="{{url('admin/get-edit-blog/'.$id)}}" method="post" onsubmit="event.preventDefault();validateMyForm();" enctype="multipart/form-data">
                    
                    <div class="blog-frm-main-container">
                    @csrf
                    <div id="input-container-meta" class="empty-container">
                            <div class="component-container">
                                <div class="main-comp-sec">
                                    <b>Meta Title</b><input type="text" name="metatitle" value="{{$blog->meta_title}}" /><br />
                                    <b>Meta Description</b><input type="text" name="metadesc" value="{{$blog->meta_desc}}" /><br />
                                    <b>Meta Tags</b><input type="text" name="metatags" value="{{$blog->meta_tags}}" />
                                </div>
                            </div>
                    </div>
                    @for($i = 0;$i < sizeof($comp);$i++)
                    <?php
                    $type = $comp[$i]->type;
                    $contentVal = $comp[$i]->content;
                    $contentImg = $comp[$i]->img;
                    ?>
                    @include('nanirj.blog-component')
                    
                    @if($i == $lastIndex)
                    <div id="options-container{{$compIndex}}" class="empty-container">
                                    <div class="component-container">
                                        <div class="add-comp-ctrl-sec">
                                        <button onclick="getBlogComponent('h1',{{$compIndex}})" class="ctrl-btn">H1</button>
                                        <button onclick="getBlogComponent('h2',{{$compIndex}})" class="ctrl-btn">H2</button>
                                        <button onclick="getBlogComponent('h3',{{$compIndex}})" class="ctrl-btn">H3</button>
                                        <button onclick="getBlogComponent('h4',{{$compIndex}})" class="ctrl-btn">H4</button>
                                        <button onclick="getBlogComponent('txtarea',{{$compIndex}})" class="ctrl-btn">P</button>
                                        <button onclick="getBlogComponent('img',{{$compIndex}})" class="ctrl-btn"><i class="fa fa-image" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                    <div id="add-container{{$compIndex}}" class="empty-container"></div>
                    @endif
                    <?php
                        $compSeq = concatCompSeq($type."_".$compIndex,$compSeq);
                        $compIndex += 1;
                         
                    ?>

                    @endfor
                    <input type="hidden" id="compSeq" name="compSeq" value="{{$compSeq}}" />
                    <input type="hidden" id="rootUrl" value="{{url('/')}}" />
                    <input type="hidden" id="component-counter" value="1" />
                    <input type="hidden" id="toDelImg" value="" />

                    <div class="component-container">
                        <div class="add-comp-ctrl-sec">
                            <button type="submit" onclick="submitFormEdit()" class="ctrl-btn" style="width: 300px;">Save</button>
                        </div>
                    </div>
                    </div>
                    </form>
                    @endsection