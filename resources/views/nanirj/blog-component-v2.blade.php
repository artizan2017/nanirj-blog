



<?php
//%7c = '|'
$compArr = explode('|',$compStr);
$valArr = array();
if($valStr != '%') {
    $valArr = explode('|',$valStr);
}
$compIndex = 2;
?>
@for($i = 0;$i < sizeof($compArr);$i++)
<?php
$val = '';
if($compArr[$i]=="img") {
    $val = 'default.png';
}
if($i < sizeof($valArr)) {
    $val = $valArr[$i];
}

?>
@if($compArr[$i]=="h1")
<div id="input-container{{$compIndex}}" class="empty-container">
        <div class="component-container">
			<div class="main-comp-sec">
				<b>Header Text</b><input type="text" name="h1_{{$compIndex}}" value="{{$val}}" onchange="onChangeInputs()" />
			</div>
			<div class="comp-ctrl-sec">
				<button class="ctrl-btn"><i class="fa fa-times" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-down" aria-hidden="true"></i></button>
			</div>
		</div>
</div>


@endif
@if($compArr[$i]=="txtarea")
<div id="input-container{{$compIndex}}" class="empty-container">
        <div class="component-container">
			<div class="main-comp-sec">
				<b>Paragraph Text</b><br /><textarea rows="4" cols="50" name="txtarea_{{$compIndex}}" class="ptxtarea" onchange="onChangeInputs()">{{$val}}</textarea>
			</div>
			<div class="comp-ctrl-sec">
				<button class="ctrl-btn"><i class="fa fa-times" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-down" aria-hidden="true"></i></button>
			</div>
		</div>
</div>


@endif
@if($compArr[$i]=="img")
<div id="input-container{{$compIndex}}" class="empty-container">
        <div class="component-container">
			<div class="main-comp-sec">
				<img src="{{url('uploads/blogimgs/'.$val)}}" style="height: 100px;width: 100px;" />
				<br />
				<b>Add Image</b><input type="file" name="img_{{$compIndex}}" />
			</div>
			<div class="comp-ctrl-sec">
				<button class="ctrl-btn"><i class="fa fa-times" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-down" aria-hidden="true"></i></button>
			</div>
		</div>
</div>



@endif
<?php $compIndex += 1; ?>
@endfor





