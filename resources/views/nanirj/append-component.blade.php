@extends('layouts.admin-portal')
@section('content')
                        
        <form id="blog-form" action="{{url('admin/get-append-blog/'.$blogID)}}" method="post" onsubmit="event.preventDefault();validateMyForm();" enctype="multipart/form-data">
        <div class="blog-frm-main-container">
            @csrf
            <input type="hidden" id="compSeq" name="compSeq" value="" />
            <input type="hidden" id="valSeq" value="" />
            <input type="hidden" id="rootUrl" value="{{url('/')}}" />
            <input type="hidden" id="component-counter" value="1" />
            
                        @for($i = 0;$i < sizeof($comp);$i++)
						<?php
							$type = $comp[$i]->type;
							$contentVal = $comp[$i]->content;
							$contentImg = $comp[$i]->img;
						?>
                        <div style="position:relative;">
						    @include('nanirj.blog-front-component')
                            <div style="position:absolute;top:0;right:0;">
                                <a href="#" onclick="openModalDel({{$comp[$i]->id}})" style="width:35px;height:35px;font-size:25px;background-color:grey;padding:5px;color:white;">&times;</a>
                                <!--<a href="#" onclick="" style="width:35px;height:35px;font-size:25px;background-color:grey;padding:5px;color:white;"><i class="fa fa-caret-down"></i></a>
                                <a href="#" onclick="" style="width:35px;height:35px;font-size:25px;background-color:grey;padding:5px;color:white;"><i class="fa fa-caret-up"></i></a>-->
                                <a href="{{url('admin/get-edit-comp/'.$comp[$i]->id)}}" target="_blank" style="width:35px;height:35px;font-size:25px;background-color:grey;padding:5px;color:white;"><i class="fa fa-edit"></i></a>
                            </div>
                        </div>
						@endfor

            <div id="options-container1" class="empty-container">
                <div class="component-container">
                    <div class="add-comp-ctrl-sec">
                        <!--<button onclick="addComponent('h1')" class="ctrl-btn">H1</button>
                        <button onclick="addComponent('txtarea')" class="ctrl-btn">P</button>
                        <button onclick="addComponent('img')" class="ctrl-btn"><i class="fa fa-image" aria-hidden="true"></i></button> -->
                        <button onclick="getBlogComponent('h1',1)" class="ctrl-btn">H1</button>
                        <button onclick="getBlogComponent('h2',1)" class="ctrl-btn">H2</button>
                        <button onclick="getBlogComponent('h3',1)" class="ctrl-btn">H3</button>
                        <button onclick="getBlogComponent('h4',1)" class="ctrl-btn">H4</button>
                        <button onclick="getBlogComponent('txtarea',1)" class="ctrl-btn">P</button>
                        <button onclick="getBlogComponent('img',1)" class="ctrl-btn"><i class="fa fa-image" aria-hidden="true"></i></button>

                    </div>
                </div>
            </div>
            <div id="comp-container" class="empty-container"></div>
            <div id="add-container1" class="empty-container"></div>
    <div class="component-container">
        <div class="add-comp-ctrl-sec">
            <button type="submit" onclick="submitForm()" class="ctrl-btn" style="width: 300px;">Save</button>
        </div>
    </div>
        </div>
        </form>

@endsection



