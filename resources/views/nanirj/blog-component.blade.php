<?php
$presetVal = ''; $presetImg = 'default.png';
if(isset($contentVal)) {
	$presetVal = $contentVal;
}
if(isset($contentImg)) {
	$presetImg = $contentImg;
}
?>
@if($type=="h1" || $type=="h2" || $type=="h3" || $type=="h4")
<?php
$hl = substr($type, -1);
switch($hl) {
	case '2':
		$hLvl = 2;
	break;
	case '3':
		$hLvl = 3;
	break;
	case '4':
		$hLvl = 4;
	break;
	case '5':
		$hLvl = 5;
	break;
	default:
		$hLvl = 1;
	break;
}
?>
<div id="input-container{{$compIndex}}" class="empty-container">
        <div class="component-container">
			<div class="main-comp-sec">
				<b>Header Text (H{{$hLvl}})</b><input type="text" name="h{{$hLvl}}_{{$compIndex}}" value="{{$presetVal}}" onchange="onChangeInputs()" required />
			</div>
			<div class="comp-ctrl-sec">
				<button class="ctrl-btn" onclick="removeInput({{$compIndex}},'h{{$hLvl}}_{{$compIndex}}')"><i class="fa fa-times" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-down" aria-hidden="true"></i></button>
			</div>
		</div>
</div>


@endif

@if($type=="txtarea")
<div id="input-container{{$compIndex}}" class="empty-container">
        <div class="component-container">
			<div class="main-comp-sec">
				<b>Paragraph Text</b><br /><textarea rows="4" cols="50" name="txtarea_{{$compIndex}}" class="ptxtarea" onchange="onChangeInputs()" required>{{$presetVal}}</textarea>
			</div>
			<div class="comp-ctrl-sec">
				<button class="ctrl-btn" onclick="removeInput({{$compIndex}},'txtarea_{{$compIndex}}')"><i class="fa fa-times" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-down" aria-hidden="true"></i></button>
			</div>
		</div>
</div>


@endif
@if($type=="img")
<div id="input-container{{$compIndex}}" class="empty-container">
        <div class="component-container">
			<div class="main-comp-sec">
				<img src="{{url('uploads/blogimgs/'.$presetImg)}}" id="disp_{{$compIndex}}" style="height: 100px;width: 100px;" />
				@if(isset($id))
				<input type="hidden" name="compID_{{$compIndex}}" value="{{$comp[$i]->id}}" />
				@endif
				<br />
				<b>Add Image</b><input type="file" name="img_{{$compIndex}}" required />
			</div>
			<div class="comp-ctrl-sec">
				<button class="ctrl-btn" onclick="removeInput({{$compIndex}},'img_{{$compIndex}}')"><i class="fa fa-times" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
				<button class="ctrl-btn"><i class="fa fa-arrow-down" aria-hidden="true"></i></button>
			</div>
		</div>
</div>



@endif
@if(!isset($id))

<div id="options-container{{$compIndex}}" class="empty-container">
                <div class="component-container">
                    <div class="add-comp-ctrl-sec">
                        <button onclick="getBlogComponent('h1',{{$compIndex}})" class="ctrl-btn">H1</button>
						<button onclick="getBlogComponent('h2',{{$compIndex}})" class="ctrl-btn">H2</button>
                        <button onclick="getBlogComponent('h3',{{$compIndex}})" class="ctrl-btn">H3</button>
                        <button onclick="getBlogComponent('h4',{{$compIndex}})" class="ctrl-btn">H4</button>
                        <button onclick="getBlogComponent('txtarea',{{$compIndex}})" class="ctrl-btn">P</button>
                        <button onclick="getBlogComponent('img',{{$compIndex}})" class="ctrl-btn"><i class="fa fa-image" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
<div id="add-container{{$compIndex}}" class="empty-container"></div>
@endif

