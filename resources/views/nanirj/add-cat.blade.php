@extends('layouts.admin-portal')
@section('content')
        <form action="{{url('admin/get-add-category')}}" method="post" enctype="multipart/form-data">
        <div class="blog-frm-main-container">
            @csrf
            <div id="input-container-meta" class="empty-container">
                    <div class="component-container">
                        <div class="main-comp-sec">
                            <b>Category Name</b><input type="text" name="catname" /><br />
                            <b>Category Description</b><input type="text" name="catdesc" /><br />
                            <b>Slug</b><input type="text" name="catslug" />
                            <br />
                            <b>Featured Image</b><input type="file" name="catimg" />
                                
                        </div>
                    </div>
            </div>
            
    <div class="component-container">
        <div class="add-comp-ctrl-sec">
            <button type="submit" class="ctrl-btn" style="width: 300px;">Save</button>
        </div>
    </div>
        </div>
        </form>

@endsection



