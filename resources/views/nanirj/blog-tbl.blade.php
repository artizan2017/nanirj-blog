@extends('layouts.admin-portal')
@section('content')
<div class="blog-frm-main-container">
<table class="table">
<thead>
    <th>Title</th>
    <th>Date</th>
    <!--<th>Edit</th>-->
    <th>Append</th>
    <th>Edit Meta</th>
    <th>Delete</th>
</thead>
<tbody>
@foreach($blogs as $blog)
<tr><td>{{$blog->name}}</td><td>{{$blog->created_at}}</td><td><a href="{{url('admin/get-append-blog/'.$blog->id)}}">Append</a></td><td><a href="{{url('admin/get-edit-meta/'.$blog->id)}}">Edit</a></td><!--<td><a href="{{url('admin/get-edit-blog/'.$blog->id)}}">Edit</a></td>--><td><a href="{{url('admin/get-destroy-blog/'.$blog->id)}}">Delete</a></td></tr>
@endforeach
</tbody>
</table>
</div>
@endsection