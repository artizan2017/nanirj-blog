@extends('layouts.admin-portal')
@section('content')
<div class="blog-frm-main-container">
<table class="table">
<thead>
    <th>Category</th>
    <th>Edit</th>
    <th>Delete</th>
</thead>
<tbody>
@foreach($cats as $cat)
<tr><td>{{$cat->name}}</td><td><a href="{{url('admin/get-edit-category/'.$cat->id)}}">Edit</a></td><td><a href="{{url('admin/get-destroy-category/'.$cat->id)}}">Delete</a></td></tr>
@endforeach
</tbody>
</table>
</div>
@endsection