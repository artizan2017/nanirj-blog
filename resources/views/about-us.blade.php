@extends('layouts.front-layout')
@section('content')
	<!-- breadcrumb -->
	<div class="breadcrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<ol class="breadcrumb" style="justify-content: flex-end;">
						<li>About Us<i class="fa fa-angle-right"></i></li>
						<li>
							<i class="fa fa-angle-left"></i>
							<a href="#">الرئيسية</a>
								<i class="fa fa-home"></i>
						</li>
						
						
					</ol>		
				</div>
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<!-- breadcrumb end -->

	<section class="main-content pt-0">
		<div class="container">
			<div class="row ts-gutter-30">
			
			<div class="col-lg-4">
					<div class="sidebar">
					@include('partials.social-widget')

						<div class="sidebar-widget ads-widget mt-20">
							<div class="ads-image">
								<a href="#">
									<img class="img-fluid" src="images/banner-image/image2.png" alt="">
								</a>
							</div>
						</div><!-- widget end -->


						


						@include('partials.tab-blogs')

						@include('partials.category-list')
						
					</div>
				</div><!-- sidebar col end -->
			
			
				<div class="col-lg-8">
					<div class="single-post">
						<div class="post-header-area">
							<h2 class="post-title title-lg pull-right-txt">About Us</h2>
							<ul class="post-meta">
								<li>
									<a href="#" class="post-cat fashion">About Us</a>
								</li>
								
								<li><a href="#"><i class="fa fa-clock-o"></i> 2019</a></li>
								<!--<li><a href="#"><i class="fa fa-comments"></i>5 Comments</a></li>
								<li><a href="#" class="view"><i class="icon icon-fire"></i> 354k</a></li>
								<li><a href="#"><i class="fa fa-eye"></i>3 minutes read</a></li>-->
								<li class="social-share">
									<i class="shareicon fa fa-share"></i>
									<ul class="social-list">
										<li><a data-social="facebook" class="facebook" href="#" title="The billionaire Philan thropist read to learn more and city"><i class="fa fa-facebook"></i></a></li>
										<li><a data-social="twitter" class="twitter" href="#" title="The billionaire Philan thropist read to learn more and city"><i class="fa fa-twitter"></i></a></li>
										<li><a data-social="linkedin" class="linkedin" href="#" title="The billionaire Philan thropist read to learn more and city"><i class="fa fa-linkedin"></i></a></li>
										<li><a data-social="pinterest" class="pinterest" href="#" title="The billionaire Philan thropist read to learn more and city"><i class="fa fa-pinterest-p"></i></a></li>
									</ul>
								</li>
							</ul>
						</div><!-- post-header-area end -->
						<div class="post-content-area pull-right-txt" style="display:flex;justify-content:flex-end;">
							<div style="width:80%;">
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                </p>
							</div>
						</div><!-- post-content-area end -->
						<div class="post-footer">
							<div class="tag-lists">
								<span>Tags: </span><a href="#">About Us</a>
							</div><!-- tag lists -->
							<div class="post-navigation clearfix">
								<div class="post-previous float-left">
									<a href="{{url('/'.$prev->slug)}}">
										<img src="{{url('uploads/blogimgs/'.$prev->featureImg)}}" alt="">
										<span>اقراء السابق</span>
										<p>{{$prev->meta_title}}</p>
									</a>
								</div>
								<div class="post-next float-right">
									<a href="{{url('/'.$next->slug)}}">
										<img src="{{url('uploads/blogimgs/'.$next->featureImg)}}" alt="">
										<span>اقراء التالي</span>
										<p>{{$next->meta_title}}</p>
									</a>
								</div>
							</div><!-- post navigation -->
							<div class="gap-30"></div>
							
						</div>
					</div><!-- single-post end -->
				</div><!-- col-lg-8 -->
				
			</div><!-- row end -->
		</div><!-- container end -->
	</section><!-- category-layout end -->
    @endsection