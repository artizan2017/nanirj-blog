<div class="sidebar-widget social-widget">
							<h2 class="block-title">
								<span class="title-angle-shap"> كن صديقا</span>
							</h2>
							<div class="sidebar-social">
								<ul class="ts-social-list">
                                    <li class="ts-facebook pull-right-txt">
										<a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" target="_blank" class="flex-box-rt">
											
											<div class="count">
												<b>Facebook</b>
												<span>Share Now!</span>
											</div>
											<i class="tsicon fa fa-facebook"></i>
										</a>
									</li>
                                    <li class="ts-instagram pull-right-txt">
										<a href="https://www.instagram.com/?url={{url()->current()}}" target="_blank" class="flex-box-rt">
											
											<div class="count">
												<b>Instagram</b>
												<span>Share Now!</span>
											</div>
											<i class="tsicon fa fa-instagram"></i>
										</a>
									</li>
									
									<li class="ts-twitter pull-right-txt">
										<a href="http://twitter.com/share?text=Nanirj&url={{url()->current()}}&hashtags=hashtag1,hashtag2,hashtag3" target="_blank" class="flex-box-rt">
											
											<div class="count">
												<b>Twitter</b>
												<span>Share Now!</span>
											</div>
											<i class="tsicon fa fa-twitter"></i>
										</a>
									</li>
                                    
									
								</ul><!-- social list -->
							</div>
						</div><!-- widget end -->