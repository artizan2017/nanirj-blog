                        <div class="sidebar-widget">
							<div class="ts-category">
								<ul class="ts-category-list pull-right-txt">
                                    @foreach($allCats as $_cat)
									<li>
										<a href="{{url('category/'.$_cat->slug)}}" style="background-image:url({{url('uploads/catimgs/'.$_cat->featureImg)}})">
											
											<span class="category-count">95</span>
											<span class="bar"></span> 
											<span> {{$_cat->name}} </span>
										</a>
									</li><!-- end list 1 -->
									
                                    @endforeach
								</ul>
							</div>
						</div><!-- widget end -->