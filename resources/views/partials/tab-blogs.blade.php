<div class="sidebar-widget featured-tab post-tab">
							<ul class="nav nav-tabs">
								<li class="nav-item">
									<a class="nav-link animated active fadeIn" href="#post_tab_a" data-toggle="tab">
										<span class="tab-head">
											<span class="tab-text-title">جديد</span>					
										</span>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link animated fadeIn" href="#post_tab_b" data-toggle="tab">
										<span class="tab-head">
											<span class="tab-text-title">مميز</span>					
										</span>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link animated fadeIn" href="#post_tab_c" data-toggle="tab">
										<span class="tab-head">
											<span class="tab-text-title">عشوائي</span>					
										</span>
									</a>
								</li>
							</ul>
							<div class="gap-50 d-none d-md-block"></div>
							<div class="row">
								<div class="col-12">
									<div class="tab-content">


										<div class="tab-pane active animated fadeInRight" id="post_tab_a">
											<div class="list-post-block">
												<ul class="list-post pull-right-txt">
                                                    @foreach($recentBlogs as $blog)
													<li>
														<div class="post-block-style media">
															<div class="post-content media-body">
																<div class="grid-category">
																	<a class="post-cat tech-color" href="{{url('category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
																</div>
																<h2 class="post-title">
																	<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
																</h2>
																<div class="post-meta mb-7">
																	<span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
																</div>
															</div><!-- Post content end -->
															<div class="post-thumb">
																<a href="{{url('/'.$blog->slug)}}">
																	<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="">
																</a>
																<span class="tab-post-count"> 1</span>
															</div><!-- Post thumb end -->
														</div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                    @endforeach
													
												</ul><!-- List post end -->
											</div>
                                        </div><!-- Tab pane 1 end -->
                                        

										<div class="tab-pane animated fadeInRight" id="post_tab_b">
											<div class="list-post-block">
												<ul class="list-post pull-right-txt">
                                                @foreach($popularBlogs as $blog)
													<li>
														<div class="post-block-style media">
															<div class="post-content media-body">
																<div class="grid-category">
																	<a class="post-cat tech-color" href="{{url('category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
																</div>
																<h2 class="post-title">
																	<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
																</h2>
																<div class="post-meta mb-7">
																	<span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
																</div>
															</div><!-- Post content end -->
															<div class="post-thumb">
																<a href="{{url('/'.$blog->slug)}}">
																	<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="">
																</a>
																<span class="tab-post-count"> 1</span>
															</div><!-- Post thumb end -->
														</div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                    @endforeach
												</ul><!-- List post end -->
											</div>
                                        </div><!-- Tab pane 2 end -->
                                        

										<div class="tab-pane animated fadeInRight" id="post_tab_c">
											<div class="list-post-block">
												<ul class="list-post pull-right-txt">
                                                @foreach($commentBlogs as $blog)
													<li>
														<div class="post-block-style media">
															<div class="post-content media-body">
																<div class="grid-category">
																	<a class="post-cat tech-color" href="{{url('category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
																</div>
																<h2 class="post-title">
																	<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
																</h2>
																<div class="post-meta mb-7">
																	<span class="post-date"><i class="fa fa-clock-o"></i> 29 July, 2020</span>
																</div>
															</div><!-- Post content end -->
															<div class="post-thumb">
																<a href="{{url('/'.$blog->slug)}}">
																	<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="">
																</a>
																<span class="tab-post-count"> 1</span>
															</div><!-- Post thumb end -->
														</div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                    @endforeach
												</ul><!-- List post end -->
											</div>
                                        </div><!-- Tab pane 2 end -->
                                        

									</div><!-- tab content -->
								</div>
							</div>
						</div><!-- widget end -->