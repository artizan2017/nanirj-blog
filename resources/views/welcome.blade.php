@extends('layouts.front-layout')
@section('head')
<meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:type" content="article" />
<meta property="og:image" content="{{url('uploads/blogimgs/rand1.jpg')}}">
    <meta property="og:title" content="Nanirj.com" />
    <meta property="og:description" content="Nanirj is where you will find the best blogs in Arabic" />
@endsection
@section('content')
	<section class="featured-post-area no-padding">
		<div class="container">
			<div class="row ts-gutter-20">
			
			<div class="col-lg-5 col-md-12 pad-l">
					<div class="row ts-gutter-20">
						<div class="col-md-12">
							<div class="post-overaly-style post-md ml-8" style="background-image:url({{url('uploads/blogimgs/'.$L1Blog->featureImg)}})">
								<div class="post-content pull-right-txt width-100">
										<a class="post-cat lifestyle" href="{{url('/category/'.$L1Blog->catSlug)}}">{{$L1Blog->catName}}</a>
										<h2 class="post-title title-md">
											<a href="{{url('/'.$L1Blog->slug)}}">{{$L1Blog->meta_title}}</a>
										</h2>
										<div class="post-meta">
											<ul>
												<li><a href="#"><i class="icon icon-clock"></i> {{$L1Blog->bdate}}</a></li>
											</ul>
										</div>
									</div><!-- Post content end -->
							</div><!-- Post Overaly end -->
						</div><!-- Col end -->
						<div class="col-md-12">
							<div class="post-overaly-style post-sm overlay-primary  ml-8 pull-right-txt" style="background-image:url({{url('uploads/blogimgs/'.$L2Blog->featureImg)}})">
								<div class="post-content pull-right-txt width-100">
										<a class="post-cat health" href="{{url('/'.$L2Blog->slug)}}">{{$L2Blog->catName}}</a>
										<h2 class="post-title title-md">
											<a href="{{url('/'.$L2Blog->slug)}}">{{$L2Blog->meta_title}}</a>
										</h2>
										<div class="post-meta">
											<ul>
												<li><a href="#"><i class="icon icon-clock"></i> {{$L1Blog->bdate}}</a></li>
											</ul>
										</div>
									</div><!-- Post content end -->
							</div><!-- Post Overaly end -->
						</div><!-- Col end -->
					</div><!-- row end -->
				</div><!-- Col 5 end -->
			
				<div class="col-lg-7 col-md-12 pad-r">
					<div id="featured-slider" class="owl-carousel owl-theme featured-slider">
                    @foreach($blogCarousel as $blog)
						<div class="item post-overaly-style" style="background-image:url({{url('uploads/blogimgs/'.$blog->featureImg)}})">
							<div class="featured-post">
								<a href="{{url('/'.$blog->slug)}}" class="image-link">&nbsp;</a>
								<div class="overlay-post-content">
									<div class="post-content width-100">
										<div class="grid-category pull-right-txt">
											<a class="post-cat fashion" href="#">{{$blog->catName}}</a>
										</div>

										<h2 class="post-title title-lg pull-right-txt">
											<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
										</h2>
										<div class="post-meta pull-right-txt">
											<ul>
												<li><a href="#"><i class="icon icon-clock"></i> {{$blog->bdate}}</a></li>
											</ul>	
										</div>
									</div>
								</div>
							</div><!--/ Featured post end -->
						</div><!-- Item 1 end -->
						@endforeach
					</div><!-- Featured owl carousel end-->
				</div><!-- Col 7 end -->

				

			</div><!-- Row end -->
		</div><!-- Container end -->
	</section><!-- Feature post end -->

	<!-- post wraper start-->
	<section class="trending-slider pb-0">
		<div class="container">
			<div class="ts-grid-box">
				<h2 class="block-title">
					 <span class="title-angle-shap"> المميز</span>
				</h2>
				<div class="owl-carousel dot-style2" id="trending-slider">
                @foreach($trendBlogs as $blog)
					<div class="item post-overaly-style post-md pull-right-txt" style="background-image:url({{url('uploads/blogimgs/'.$blog->featureImg)}})">
						<a href="{{url('/'.$blog->slug)}}" class="image-link">&nbsp;</a>
						<div class="overlay-post-content">
							<div class="post-content width-100">
								<div class="grid-category">
									<a class="post-cat sports" href="#">{{$blog->catName}}</a>
								</div>

								<h2 class="post-title">
									<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
								</h2>
								<div class="post-meta">
									<!--<ul>
										<<li><a href="#"><i class="fa fa-user"></i> Admin</a></li>
									</ul>-->
								</div>
							</div>
						</div>
					</div><!-- Item 1 end -->
					@endforeach
				</div>
				<!-- most-populers end-->
			</div>
			<!-- ts-populer-post-box end-->
		</div>
		<!-- container end-->
	</section>
	<!-- .section -->

	<section class="block-wrapper">
		<div class="container">
			<div class="row ts-gutter-30">
			<div class="col-lg-4">
					<div class="sidebar">
						@include('partials.social-widget')

						<div class="sidebar-widget ads-widget">
							<div class="ads-image">
								<a href="#">
									<img class="img-fluid" src="images/banner-image/image2.png" alt="">
								</a>
							</div>
						</div><!-- widget end -->
						<div class="gap-20 d-none d-lg-block"></div>
						@include('partials.tab-blogs')
					</div>
				</div><!-- Sidebar Col end -->
				<div class="col-lg-8 col-md-12">
					<!--- Featured Tab startet -->
					<div class="featured-tab">
						<h2 class="block-title">
							<span class="title-angle-shap"> جديدنا</span>
						</h2>
						<ul class="nav nav-tabs">
								
							<?php $alphabet = 'bcdefghijklmnopqrstuvwxyz'; $alphaIndex = 0; ?>
							@foreach($cats as $cat)
								<li class="nav-item">
									<a class="nav-link animated fadeIn" href="#tab_{{$alphabet[$alphaIndex]}}" data-toggle="tab">
										<span class="tab-head">
										<span class="tab-text-title">{{$cat->name}}</span>					
									</span>
									</a>
								</li>
								<?php $alphaIndex += 1; ?>
							@endforeach
								<li class="nav-item">
									<a class="nav-link animated active fadeIn" href="#tab_a" data-toggle="tab">
										<span class="tab-head">
										<span class="tab-text-title">الكل</span>					
									</span>
									</a>
								</li>
								
						</ul>

						<div class="tab-content">
							<div class="tab-pane active animated fadeInRight" id="tab_a">
								<div class="row">
									<div class="col-lg-6 col-md-6">
										<div class="post-block-style clearfix">
											<div class="post-thumb">
												<a href="{{url('/'.$TabAllMainBlog->slug)}}">
													<img class="img-fluid" src="{{url('uploads/blogimgs/'.$TabAllMainBlog->featureImg)}}" alt="" />
												</a>
												<div class="grid-cat">
													<a class="post-cat tech" href="{{url('category/'.$TabAllMainBlog->catSlug)}}">{{$TabAllMainBlog->catName}}</a>
												</div>
											</div>
											
											<div class="post-content pull-right-txt">
													<h2 class="post-title title-md">
														<a href="{{url('/'.$TabAllMainBlog->slug)}}">{{$TabAllMainBlog->meta_title}}</a>
													</h2>
													<div class="post-meta mb-7">
														<span class="post-date"><i class="fa fa-clock-o"></i> {{$TabAllMainBlog->bdate}}</span>
													</div>
													<p>{{$TabAllMainBlog->meta_desc}}</p>
												</div><!-- Post content end -->
										</div><!-- Post Block style end -->
									</div><!-- Col end -->

									<div class="col-lg-6 col-md-6">
										<div class="list-post-block">
											<ul class="list-post">
												@foreach($TabAllBlogs as $blog)
												<li>
													<div class="post-block-style media">
														<div class="post-content media-body pull-right-txt">
																<div class="grid-category">
																	<a class="post-cat tech-color" href="{{url('category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
																</div>
																<h2 class="post-title">
																	<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
																</h2>
																<div class="post-meta mb-7">
																	<span class="post-date"><i class="fa fa-clock-o"></i> {{$blog->bdate}}</span>
																</div>
																</div><!-- Post content end -->
														<div class="post-thumb">
															<a href="{{url('/'.$blog->slug)}}">
																<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="" />
															</a>
														</div><!-- Post thumb end -->

														
													</div><!-- Post block style end -->
												</li><!-- Li 1 end -->
												@endforeach
												
											</ul><!-- List post end -->
										</div><!-- List post block end -->
									</div><!-- List post Col end -->
								</div><!-- Tab pane Row 1 end -->
							</div><!-- Tab pane 1 end -->
							<?php $alphabet = 'bcdefghijklmnopqrstuvwxyz'; $alphaIndex = 0; ?>
							@for($i=0; $i < sizeof($TabCatMainBlogsArr); $i++)
							<div class="tab-pane animated fadeInRight" id="tab_{{$alphabet[$alphaIndex]}}">
								<div class="row">
									<div class="col-lg-6 col-md-6">
										<div class="post-block-style clearfix">
											<div class="post-thumb">
												<a href="{{url('/'.$TabCatMainBlogsArr[$i]->slug)}}">
													<img class="img-fluid" src="{{url('uploads/blogimgs/'.$TabCatMainBlogsArr[$i]->featureImg)}}" alt="" />
												</a>
												<div class="grid-cat">
													<a class="post-cat tech" href="{{url('category/'.$TabCatMainBlogsArr[$i]->catSlug)}}">{{$TabCatMainBlogsArr[$i]->catName}}</a>
												</div>
											</div>
											
											<div class="post-content pull-right-txt">
													<h2 class="post-title title-md">
														<a href="{{url('/'.$TabCatMainBlogsArr[$i]->slug)}}">{{$TabCatMainBlogsArr[$i]->meta_title}}</a>
													</h2>
													<div class="post-meta mb-7">
														<span class="post-author"><a href="#"><i class="fa fa-user"></i> John Doe</a></span>
														<span class="post-date"><i class="fa fa-clock-o"></i> {{$TabCatMainBlogsArr[$i]->bdate}}</span>
													</div>
													<p>{{$TabCatMainBlogsArr[$i]->meta_desc}}</p>
												</div><!-- Post content end -->
										</div><!-- Post Block style end -->
									</div><!-- Col end -->

									<div class="col-lg-6 col-md-6">
										<div class="list-post-block">
											<ul class="list-post">
												@foreach($TabCatBlogsArr[$i] as $blog)
												<li>
													<div class="post-block-style media">
														<div class="post-content media-body pull-right-txt">
																<div class="grid-category">
																	<a class="post-cat tech-color" href="{{url('category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
																</div>
																<h2 class="post-title">
																	<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
																</h2>
																<div class="post-meta mb-7">
																	<span class="post-date"><i class="fa fa-clock-o"></i> {{$blog->bdate}}</span>
																</div>
																</div><!-- Post content end -->
														<div class="post-thumb">
															<a href="{{url('/'.$blog->slug)}}">
																<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="" />
															</a>
														</div><!-- Post thumb end -->

														
													</div><!-- Post block style end -->
												</li><!-- Li 1 end -->
												@endforeach
												
											</ul><!-- List post end -->
										</div><!-- List post block end -->
									</div><!-- List post Col end -->
								</div><!-- Tab pane Row 1 end -->
							</div><!-- Tab pane 1 end -->
							<?php $alphaIndex += 1; ?>
						@endfor
							
						</div><!-- tab content -->
					</div><!-- Technology Tab end -->

					<div class="gap-20"></div>

					<div class="block style2 text-light">
						<h2 class="block-title pull-right-txt">
							<span class="title-angle-shap"> {{$blackPanelOtherBlogs[0]->catName}} </span>
						</h2>

						<div class="row">
							

							<div class="col-lg-6 col-md-6">
								<div class="row ts-gutter-20">
									@foreach($blackPanelOtherBlogs as $blog)
									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="{{url('/'.$blog->slug)}}">
													<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="" />
												</a>
											</div>
											
											<div class="post-content pull-right-txt">
												<h2 class="post-title mb-2">
													<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
												</h2>
												<div class="post-meta mb-7">
													<span class="post-date"><i class="fa fa-clock-o"></i>{{$blog->bdate}}</span>
												</div>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->
									@endforeach
								</div><!-- .row -->
							</div><!-- Col 2 end -->

							<div class="col-lg-6 col-md-6">
								<div class="post-block-style">
									<div class="post-thumb">
										<a href="{{url('/'.$blackPanelMainBlog->slug)}}">
											<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blackPanelMainBlog->featureImg)}}" alt="" />
										</a>
										<div class="grid-cat">
											<a class="post-cat tech" href="{{url('category/'.$blackPanelMainBlog->catSlug)}}">{{$blackPanelMainBlog->catName}}</a>
										</div>
									</div>
									
									<div class="post-content pull-right-txt">
											<h2 class="post-title title-md">
												<a href="{{url('/'.$blackPanelMainBlog->slug)}}">{{$blackPanelMainBlog->meta_title}}</a>
											</h2>
											{!!$blackPanelMainBlog->meta_desc!!}
											<div class="post-meta mb-7">
											<span class="post-author"><a href="#"><i class="fa fa-user"></i> John Doe</a></span>
											<span class="post-date"><i class="fa fa-clock-o"></i>{{$blackPanelMainBlog->bdate}}</span>
										</div>
										</div><!-- Post content end -->
								</div><!-- Post block a end -->
							</div><!-- Col 1 end -->

						</div><!-- Row end -->
					</div><!-- Block Lifestyle end -->

	


				</div><!-- Content Col end -->

				
			</div><!-- Row end -->
		</div><!-- Container end -->
	</section><!-- First block end -->

					<!-- block slider -->
					<section class="block-slider">
		<div class="container">
			<div class="ts-grid-box">
				<div class="owl-carousel dot-style2" id="post-block-slider">
					@foreach($blockSliderBlogs as $blog)
					<div class="item">
						<div class="post-block-style">
							<div class="post-thumb">
								<a href="{{url('/'.$blog->slug)}}">
									<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="" />
								</a>
								<div class="grid-cat">
									<a class="post-cat tech" href="{{url('category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
								</div>
							</div>
							
							<div class="post-content pull-right-txt">
								<h2 class="post-title">
									<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i>{{$blog->bdate}}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post Block style end -->
					</div><!-- slide-item end -->
					@endforeach
					
				</div>
			</div>
			<!-- ts-populer-post-box end-->
		</div>
		<!-- container end-->
	</section>
	<!-- .block slider -->

	<section class="block-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12">
					<h2 class="block-title pull-right-txt">
						<span class="title-angle-shap"> {{$whitePanelMainBlog->catName}} </span>
					</h2>
					<div class="post-block-style">
						<div class="row align-items-center">
							
							<div class="col-md-6">
								<div class="post-content pull-right-txt">
									<h2 class="post-title title-md">
										<a href="{{url('/'.$whitePanelMainBlog->slug)}}">{{$whitePanelMainBlog->meta_title}}</a>
									</h2>
									<div class="post-meta mb-7">
										<span class="post-author"><a href="#"><i class="fa fa-user"></i> John Doe</a></span>
										<span class="post-date"><i class="fa fa-clock-o"></i> {{$whitePanelMainBlog->bdate}}</span>
									</div>
									{!!$whitePanelMainBlog->meta_desc!!}
								</div>
							</div>

							<div class="col-md-6">
								<div class="post-thumb">
									<img src="{{url('uploads/blogimgs/'.$whitePanelMainBlog->featureImg)}}" alt="">
								</div>
							</div>
						</div>
					</div>
					
					<div class="gap-30"></div>

					<div class="row">
						<div class="col-md-6">
							<div class="list-post-block">
								<ul class="list-post">
									@foreach($whitePanelOtherBlogs1 as $blog)
									<li>
										<div class="post-block-style media">
										<div class="post-content media-body pull-right-txt">
												<div class="grid-category">
													<a class="post-cat tech-color" href="{{url('category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
												</div>
												<h2 class="post-title">
													<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
												</h2>
												<div class="post-meta mb-7">
													<span class="post-date"><i class="fa fa-clock-o"></i>{{$blog->bdate}}</span>
												</div>
												</div><!-- Post content end -->
											<div class="post-thumb">
												<a href="{{url('/'.$blog->slug)}}">
													<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="">
												</a>
											</div><!-- Post thumb end -->
	
											
										</div><!-- Post block style end -->
									</li><!-- Li 1 end -->
									@endforeach
									
								</ul><!-- list-post end -->
							</div>
						</div><!-- col end -->
						<div class="col-md-6">
							<div class="list-post-block">
								<ul class="list-post">
									@foreach($whitePanelOtherBlogs2 as $blog)
										<li>
											<div class="post-block-style media">
											<div class="post-content media-body pull-right-txt">
													<div class="grid-category">
														<a class="post-cat tech-color" href="{{url('category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
													</div>
													<h2 class="post-title">
														<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
													</h2>
													<div class="post-meta mb-7">
														<span class="post-date"><i class="fa fa-clock-o"></i>{{$blog->bdate}}</span>
													</div>
													</div><!-- Post content end -->
												<div class="post-thumb">
													<a href="{{url('/'.$blog->slug)}}">
														<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="">
													</a>
												</div><!-- Post thumb end -->
		
												
											</div><!-- Post block style end -->
										</li><!-- Li 1 end -->
										@endforeach
								</ul><!-- list-post end -->
							</div>
						</div><!-- col end -->
					</div>
				</div><!-- Content Col end -->
				<div class="col-lg-4 col-md-12">
				<div class="ts-category">
								<ul class="ts-category-list pull-right-txt">
                                    @foreach($allCats as $_cat)
									<li>
										<a href="{{url('category/'.$_cat->slug)}}" style="background-image:url({{url('uploads/catimgs/'.$_cat->featureImg)}})">
											
											<span class="category-count">95</span>
											<span class="bar"></span> 
											<span> {{$_cat->name}} </span>
										</a>
									</li><!-- end list 1 -->
									
                                    @endforeach
								</ul>
							</div>
				</div><!-- Sidebar Col end -->
			</div><!-- Row end -->
		</div><!-- Container end -->
	</section><!-- First block end -->

	<!-- post wraper start-->
	<section class="trending-slider full-width no-padding">
		<div class="ts-grid-box">
			<div class="owl-carousel" id="fullbox-slider">
				@foreach($bigSliderBlogs as $blog)
				<div class="item post-overaly-style post-lg" style="background-image:url({{url('uploads/blogimgs/'.$blog->featureImg)}})">
					<a href="{{url('/'.$blog->slug)}}" class="image-link">&nbsp;</a>
					<div class="overlay-post-content">
						<div class="post-content pull-right-txt width-100">
							<div class="grid-category">
								<a class="post-cat lifestyle" href="{{url('category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
							</div>

							<h2 class="post-title title-md">
								<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
							</h2>
							<div class="post-meta">
								<ul>
									<li class="post-date"><i class="fa fa-clock-o"></i> {{$blog->bdate}}</li>
								</ul>
							</div>
						</div>
					</div>
				</div><!-- Item 1 end -->
				@endforeach
			</div>
			<!-- most-populers end-->
		</div>
	</section>


	<section class="block-wrapper">
		<div class="container">
			<div class="row ts-gutter-30">
			<div class="col-lg-4 col-md-12">
					<h2 class="block-title block-title-dark pull-right-txt">
						<span class="title-angle-shap">احدث المواضيع </span>
					</h2>
					<div class="list-post-block">
						<ul class="list-post">
							@foreach($recentBlogs as $blog)
							<li>
								<div class="post-block-style media">
									<div class="post-content media-body pull-right-txt">
										<div class="grid-category">
											<a class="post-cat tech-color" href="{{url('category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
										</div>
										<h2 class="post-title">
											<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
										</h2>
										<div class="post-meta mb-7">
											<span class="post-date"><i class="fa fa-clock-o"></i> {{$blog->bdate}}</span>
										</div>
									</div><!-- Post content end -->
									<div class="post-thumb thumb-md">
										<a href="{{url('/'.$blog->slug)}}">
											<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="">
										</a>
									</div><!-- Post thumb end -->

									
								</div><!-- Post block style end -->
							</li><!-- Li 1 end -->
							@endforeach
							
						</ul><!-- List post end -->
					</div>
				</div><!-- Sidebar Col end -->
				<div class="col-lg-8 col-md-12">
					<h2 class="block-title pull-right-txt">
						<span class="title-angle-shap"> شاهد ايضاً </span>
					</h2>
					<div class="row text-light ts-gutter-30">
						@foreach($dontMissBlogs as $blog)
						<div class="col-md-4">
							<div class="post-overaly-style post-sm" style="background-image:url({{url('uploads/blogimgs/'.$blog->featureImg)}})">
								<a href="{{url('/'.$blog->slug)}}" class="image-link">&nbsp;</a>
								<div class="overlay-post-content pull-right-txt width-100">
									<div class="post-content">
										<div class="grid-category">
											<a class="post-cat tech" href="{{url('category/'.$blog->catSlug)}}">{{$blog->catName}}</a>
										</div>
		
										<h2 class="post-title mb-0">
											<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
										</h2>
									</div>
								</div>
							</div><!-- post end -->
						</div><!-- end col -->
						@endforeach
						
					</div><!-- end row -->
					
				</div><!-- Content Col end -->
				
			</div><!-- Row end -->
		</div><!-- Container end -->
	</section><!-- First block end -->

	<!-- ad banner start-->
	<div class="block-wrapper no-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="banner-img text-center">
						<a href="index.html">
							<img class="img-fluid" src="{{url('assets/images/banner-image/image3.png')}}" alt="">
						</a>
					</div>
				</div>
				<!-- col end -->
			</div>
			<!-- row  end -->
		</div>
		<!-- container end -->
	</div>
	<!-- ad banner end-->

	
    <div class="gap-50"></div>
    @endsection

