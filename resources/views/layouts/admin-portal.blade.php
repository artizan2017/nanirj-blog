<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{url('css/blog-form.css')}}">
        <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<style>
		@import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
		body {
			padding: 0px;
			margin: 0px;
		}
		.admin-portal-main-new {
			width: 100%;
			background-color: #FAFAFA;
			display: flex;
			align-items: center;
			flex-direction: column;
			font-family: 'Roboto', sans-serif;
			padding: 0px 0px; 
			height: 100%;
        }
		.ap-new-header {
			background-color: #f5f5f5;
			width: 100%;
			height: 70px;
			display: flex;
		}
		.ap-h-sec1 {
			background-color: #6e6e6e;
			width: 10%;
			height: 100%;
			display: flex;
			align-items: center;
			justify-content: center;
		}
		.ap-h-sec2 {
			background-color: #a8a8a8;
			width: 90%;
			height: 100%;
			display: flex;
		}
		.ap-acc-sec {
			display: flex;
			align-items: center;
			width: 20%;
			height: 100%;
			
			margin-right: 20px;
			color: white;
		}
		.ap-acc-sec-pull-right {
			margin-left: auto;
		}
		.ap-acc-sub-sec {
			position: relative;
			display: flex;
			align-items: center;
			margin-left: auto;
			height: 100%;
		}
		
		.ap-acc-sub-sec > img {
			border-radius: 50%;
		}
		.ap-acc-sec > span {

		}
		.ap-acc-sub-sec:hover .ap-new-acc-dropdown {
			display: flex;
		}
		.ap-new-acc-dropdown:hover .ap-new-acc-dropdown {
			display: flex;
		}
		.ap-new-acc-dropdown {
			position: absolute;
			z-index: 1;
			top: 70px;
			display: none;
			background-color: #f7f7f7;
			color: black;
			border: 1px solid #e0e0e0;
			padding: 10px 10px;
			margin-left: auto;
			width: 100%;
			justify-content: center;
		}
		.ap-new-acc-dropdown > ul {
			list-style: none;
			margin: 0px;
			padding: 0px;
		}
		.ap-new-body {
			background-color: #f7f7f7;
			width: 100%;
			height: 100%;
			display: flex;
		}
		.ap-new-sidebar {
			background-color: #7a7a7a;
			
			width: 10%;
		}
		.ap-new-menu-list {
			list-style: none;
			padding-left: 0px;
			margin: 0px;
		}
		.ap-new-menu-list > li {
			
		}
		.ap-new-menu-list > li > a {
			text-decoration: none;
			color: #dedede;
			width: 100%;
			display: flex;
			align-items: center;
		}
		.ap-new-menu-list > li > a > i {
			font-size: 30px;
			margin-right: 10px;
			margin-left: 20px;
		}
		.ap-new-menu-list > li {
			padding: 15px 0px;
		}
		.ap-new-menu-list > li:hover {
			background-color: #5492F7;
		}
		.ap-new-content {
			background-color: #e3e3e3;
			width: 90%;
			overflow-y: scroll;
			height: 870px;
		}
		</style>
	</head>
	<body>
	<div class="admin-portal-main-new">
		<div class="ap-new-header">
			<div class="ap-h-sec1"><!-- <img src="buildeey-logo.png" height="30px" /> --></div>
			<div class="ap-h-sec2">
				
				
				<div class="ap-acc-sec ap-acc-sec-pull-right">
				<div class="ap-acc-sub-sec">
					<span>Hi {{Auth::user()->name}}!</span>
					<span style="width:10px;"></span>
					<img src="{{url('assets/images/anon-person.jpg')}}" height="30px" width="30px" />

					<div class="ap-new-acc-dropdown">
					<ul>
						<li><a href="{{ url('admin/logout') }}">Sign-Out</a></li>
					</ul>
					</div>

				</div>
				</div>
			</div>
		</div>
		<div class="ap-new-body">
			<div class="ap-new-sidebar">
				<ul class="ap-new-menu-list">
					<li><a href="{{url('admin/blogs')}}"><i class="fa fa-file-text-o"></i><span>Blogs</span></a></li>
					<li><a href="{{url('admin/get-add-blog')}}"><i class="fa fa-file-text-o"></i><span>Add Blog</span></a></li>
					<li><a href="{{url('admin/categories')}}"><i class="fa fa-file-text-o"></i><span>Categories</span></a></li>
					<li><a href="{{url('admin/get-add-category')}}"><i class="fa fa-file-text-o"></i><span>Add Category</span></a></li>
				</ul>
			</div>
			<div class="ap-new-content" style="position:relative;">
				@include('partials.valid-msg-admin');
				@yield('content')
				<div id="msgModal" style="position:fixed;background-color:rgba(5,5,5,0.5);width:100%;height:100%;z-index:3;top:0;left:0;display:flex;justify-content:center;align-items:center;display:none;">
					<div style="width:50%;background-color:white;display:flex;flex-direction:column;">
						<div style="width:100%;display:flex;justify-content:flex-end;">
							<a href="#" onclick="closeModal()" style="width:35px;height:35px;font-size:25px;background-color:grey;padding:5px;color:white;">&times;</a>
						</div>
						<div style="width:100%;padding:15px;">
							Are you sure you want to delete this component?
						</div>
						<div style="width:100%;padding:15px;display:flex;">
							<input type="hidden" id="compID" />
							<button onclick="destroyComp()">Yes</button><button onclick="closeModal()">No</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script>
    tinymce.init({
    selector: 'textarea.ptxtarea',
    });
    </script>
	<script>
		function closeModal() {
			document.getElementById('msgModal').style.display = 'none';
		}
		function openModalDel(compID) {
			document.getElementById('compID').value = compID;
			document.getElementById('msgModal').style.display = 'flex';
		}
		
		function destroyComp() {
			var xhttp = new XMLHttpRequest();
			var compID = document.getElementById('compID').value;
			var rootUrl = document.getElementById("rootUrl").value;
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					location.reload();
				}
			};
			xhttp.open("GET", rootUrl + "/admin/get-destroy-comp/" + compID, true);
			xhttp.send();
		}
	</script>
    <script src="{{asset('jeffreyjs/nanirj.js')}}" async defer></script>
	</body>
</html>