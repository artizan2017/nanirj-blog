<!DOCTYPE html>
<html lang="en">
<head>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
	<title>Nanirj</title>

	<!-- Mobile Specific Metas
	================================================== -->

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!--Favicon-->
	<link rel="shortcut icon" href="{{url('assets/images/favicon2.ico')}}" type="image/x-icon">
	<link rel="icon" href="{{url('assets/images/favicon2.ico')}}" type="image/x-icon">
	
	<!-- CSS
	================================================== -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
	
	<!-- IconFont -->
	<link rel="stylesheet" href="{{url('assets/css/iconfonts.css')}}">
	<!-- FontAwesome -->
	<link rel="stylesheet" href="{{url('assets/css/font-awesome.min.css')}}">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{{url('assets/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{url('assets/css/owl.theme.default.min.css')}}">
	<!-- magnific -->
	<link rel="stylesheet" href="{{url('assets/css/magnific-popup.css')}}">

	
	<link rel="stylesheet" href="{{url('assets/css/animate.css')}}">

	<!-- Template styles-->
	<link rel="stylesheet" href="{{url('assets/css/style.css')}}">
	<!-- Responsive styles-->
	<link rel="stylesheet" href="{{url('assets/css/responsive.css')}}">
	
	<!-- Colorbox -->
	<link rel="stylesheet" href="{{url('assets/css/colorbox.css')}}">
	
	<link rel="stylesheet" href="{{url('assets/css/custom-style.css')}}">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	@yield('head')

</head>
	
<body>
	<div class="trending-bar trending-light d-md-block">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-md-3 text-md-left text-center">
					<div class="ts-date">
						<i class="fa fa-calendar-check-o"></i>{{date('M-d-Y')}}
					</div>
				</div><!-- Col end -->
				
				
			</div><!--/ Row end -->
		</div><!--/ Container end -->
	</div><!--/ Trending end -->

	<!-- Header start -->
	<header id="header" class="header">
		<div class="container">
		<div class="row align-items-center" style="justify-content: flex-end;">
				<!--<div class="col-md-2 col-sm-12">
					<div class="logo" style="width:100px;padding:0px;">
						 <a href="index.html">
							<img src="{{url('assets/images/logos/nanirj-logo.png')}}" height="100px" alt="">
						 </a>
					</div>
				</div><-- logo col end -->
				<div class="col-md-8 col-sm-12 header-right">
					<div class="ad-banner float-right" style="margin-bottom:10px;">
						<a href="#">
							<img src="{{url('assets/images/banner-image/image1.png')}}" class="img-fluid" alt="">
						</a>
					</div>
				</div><!-- header right end -->
				

				
			</div><!-- Row end -->
		</div><!-- Logo and banner area end -->
	</header><!--/ Header end -->

	<div class="main-nav clearfix is-ts-sticky">
		<div class="container">
			<div class="row" style="justify-content: flex-end;">
				<div class="col-lg-3 text-right nav-social-wrap" style="left:0px; display:flex; justify-content:left;">
					<div class="top-social">
						<ul class="social list-unstyled">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
					

					<div class="nav-search">
						<a href="#search-popup" class="xs-modal-popup">
							<i class="icon icon-search1"></i>
						</a>
					</div><!-- Search end -->
						
					<div class="zoom-anim-dialog mfp-hide modal-searchPanel ts-search-form" id="search-popup">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="xs-search-panel">
									<form class="ts-search-group">
										<div class="input-group">
											<input type="search" class="form-control" name="s" placeholder="Search" value="">
											<button class="input-group-btn search-button">
												<i class="icon icon-search1"></i>
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- End xs modal -->
				</div>
				
				<div style="width: 100%;"></div>
				
				<nav class="navbar navbar-expand-lg col-lg-8" style="justify-content: flex-end;">
					<div class="site-nav-inner float-left">
					<button class="navbar-toggler" style="right:0px;" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
						<span class="fa fa-bars"></span>
					</button>
					   <!-- End of Navbar toggler -->
					   <div id="navbarSupportedContent" class="collapse navbar-collapse navbar-responsive-collapse" style="justify-content: flex-end;">
							<ul class="nav navbar-nav">
								@foreach($cats as $cat)
									<li style="text-align:right;">
										<a href="{{url('category/'.$cat->slug)}}">{{$cat->name}}</a>
									</li>
								@endforeach()
								
								<li class="nav-item dropdown active">
									<a href="{{url('/')}}" style="font-size: 30px;"><i class="fa fa-home" aria-hidden="true"></i></a>
									
								</li>
								<li style="display:flex;align-items:center;">
									<a href="{{url('/')}}" style="font-size: 30px;"><img src="{{url('assets/images/logos/nanirj-w-logo.png')}}" height="50px" alt=""></a>
								</li>

								

								

								
									
							</ul><!--/ Nav ul end -->
						</div><!--/ Collapse end -->

					</div><!-- Site Navbar inner end -->
				</nav><!--/ Navigation end -->

				<!--<div class="col-lg-4 text-right nav-social-wrap">
					<div class="top-social">
						<ul class="social list-unstyled">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
					

					<div class="nav-search">
						<a href="#search-popup" class="xs-modal-popup">
							<i class="icon icon-search1"></i>
						</a>
					</div><-- Search end -->
						
					<div class="zoom-anim-dialog mfp-hide modal-searchPanel ts-search-form" id="search-popup">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="xs-search-panel">
									<form class="ts-search-group">
										<div class="input-group">
											<input type="search" class="form-control" name="s" placeholder="Search" value="">
											<button class="input-group-btn search-button">
												<i class="icon icon-search1"></i>
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- End xs modal -->
				</div>
			</div><!--/ Row end -->
		</div><!--/ Container end -->
	</div><!-- Menu wrapper end -->

	<div class="gap-30"></div>


<!-- MAIN CONTENT -->
@yield('content')

    <div class="newsletter-area">
		<div class="container">
			<div class="row ts-gutter-30 justify-content-center align-items-center">
				<div class="col-lg-5 col-md-6">
					<div class="footer-newsletter">
						<form action="#" method="post">
							<div class="email-form-group">
								<i class="news-icon fa fa-paper-plane" aria-hidden="true"></i>
								<input type="email" name="EMAIL" class="newsletter-email" placeholder="Your email" required>
								<input type="submit" class="newsletter-submit" value="اشترك معنا">
							</div>
							
						</form>
					</div>
				</div>
				<!-- col end -->
				<div class="col-lg-7 col-md-6">
					<div class="footer-loto" style="display:flex;justify-content: flex-end;">
						<a href="#">
							<img src="{{url('assets/images/logos/nanirj-w-logo.png')}}" height="80px" alt="">
						</a>
					</div>
				</div>
				<!-- col end -->
				
			</div>
			<!-- row  end -->
		</div>
		<!-- container end -->
	</div>
	<!-- ad banner end-->

	<!-- Footer start -->
	<div class="ts-footer">
		<div class="container">
			<div class="row ts-gutter-30 justify-content-lg-between justify-content-center">
				<!-- col-start -->
				<div class="col-lg-3 col-md-6">
					<div class="footer-widtet post-widget">
						<div class="widget-content">
							<div class="footer-ads">
								<a href="#">
									<img class="img-fluid" src="{{url('assets/images/banner-image/image6.jpg')}}" alt="">
								</a>
							</div>
						</div>
					</div>
				</div><!-- col end -->
				<div class="col-lg-3 col-md-6">
					<div class="footer-widtet post-widget">
						<h3 class="widget-title pull-right-txt"><span>اكثر قرائه </span></h3>
						<div class="widget-content">
							<div class="list-post-block">
								<ul class="list-post">
									@foreach($popularBlogs as $blog)
									<li>
										<div class="post-block-style media">
											
	
											<div class="post-content media-body pull-right-txt">
												<h4 class="post-title">
													<a href="{{url('/'.$blog->slug)}}">{{$blog->meta_title}}</a>
												</h4>
												<div class="post-meta mb-7">
													<span class="post-date"><i class="fa fa-clock-o"></i> {{$blog->bdate}}</span>
												</div>
												</div><!-- Post content end -->
											<div class="post-thumb">
												<a href="{{url('/'.$blog->slug)}}">
													<img class="img-fluid" src="{{url('uploads/blogimgs/'.$blog->featureImg)}}" alt="">
												</a>
											</div><!-- Post thumb end -->
										</div><!-- Post block style end -->
									</li><!-- Li 1 end -->
									@endforeach
								</ul><!-- list-post end -->
							</div>
						</div>
					</div>
				</div><!-- col end -->
				<div class="col-lg-4 col-md-6">
					<div class="footer-widtet">
						<h3 class="widget-title pull-right-txt"><span>نانرج</span></h3>
						<div class="widget-content pull-right-txt">
							<p>نانرج موقع عربي شامل ومتنوع  يزودكم بكل ما هو جديد وشيق من احداث ومواضيع عالمية </p>
							<ul class="ts-footer-info">
								<!--<li><i class="fa fa-home"></i> 15 Cliff St, New York NY 10038, USA</li>
								<li><i class="icon icon-phone2"></i> +1 212-602-9641</li>
								<li><i class="fa fa-envelope"></i>info@example.com</li>-->
							</ul>
							<ul class="ts-social">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-instagram"></i></a></li>
							</ul>
						</div>
					</div>
				</div><!-- col end -->
				
				
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<!-- Footer End-->

	<!-- ts-copyright start -->
	<div class="ts-copyright">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-12 text-center">
					<div class="copyright-content text-light">
						<p>&copy; جميع الحقوق محفوظة لموقع نانرج © 2019</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ts-copyright end-->

	<!-- backto -->
	<div class="top-up-btn">
		<div class="backto" style="display: block;"> 
			<a href="#" class="icon icon-arrow-up" aria-hidden="true"></a>
		</div>
	</div>
	<!-- backto end-->
	
	<!-- Javascript Files
	================================================== -->

	<!-- initialize jQuery Library -->
	<script src="{{url('assets/js/jquery.js')}}"></script>
	<!-- Popper Jquery -->
	<script src="{{url('assets/js/popper.min.js')}}"></script>
	<!-- Bootstrap jQuery -->
	<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
	<!-- magnific-popup -->
	<script src="{{url('assets/js/jquery.magnific-popup.min.js')}}"></script>
	<!-- Owl Carousel -->
	<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
	<!-- Color box -->
	<script src="{{url('assets/js/jquery.colorbox.js')}}"></script>
	<!-- Template custom -->
	<script src="{{url('assets/js/custom.js')}}"></script>
	
</body>
</html>