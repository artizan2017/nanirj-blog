function getBlogComponent(type,compIndex) {
    var xhttp = new XMLHttpRequest();
    var nextCompIndex = compIndex + 1;
    var rootUrl = document.getElementById("rootUrl").value;
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
       document.getElementById("add-container" + compIndex).innerHTML = this.responseText;
       document.getElementById("options-container" + compIndex).innerHTML = "";
       addToCompSeq(type,nextCompIndex);
       tinymce.init({
        selector: 'textarea.ptxtarea',
        init_instance_callback: function (editor) {
          editor.on('keyup', function (e) {
            onChangeInputs();
          });
        }
       });
      }
    };
    xhttp.open("GET", rootUrl + "/admin/get-add-component/" + type + "/" + nextCompIndex, true);
    xhttp.send();
  }
  
  function addToCompSeq(type,compIndex) {
      var compSeqInput = document.getElementById("compSeq");
      if(compSeqInput.value == "") {
        compSeqInput.value += subCompToAdd(type,compIndex);
      }else{
        compSeqInput.value += "," + subCompToAdd(type,compIndex);
      }
  }
  function subCompToAdd(type,compIndex) {
      var retVal = "";
      retVal = type + "_" + compIndex;
      return retVal;
  }
  function validateMyForm()
{
  
    if(1 === 2)
    { 
      return true;
    }else{
        return false;
    }
  

}
function submitForm() {
  var inputs = document.getElementById("compSeq").value;
  var inputsArr = inputs.split(',');
  var isComplete = true;
  for(var i = 0;i < inputsArr.length;i++) {
    var nameParts = inputsArr[i].split('_');
    if(nameParts[0] != 'txtarea'){
      if(document.getElementsByName(inputsArr[i])[0].value == '' || document.getElementsByName(inputsArr[i])[0].value == null) {
        alert(inputsArr[i]);
        isComplete = false;
      }
    }
  }
  if(isComplete){
    //document.getElementById("pls-wait").innerHTML = "Loading..."
    tinymce.triggerSave();
    document.getElementById("blog-form").submit();
  }else{
    alert('Please fill-out everything');
  }
}
function submitFormEdit() {
  var inputs = document.getElementById("compSeq").value;
  var inputsArr = inputs.split(',');
  var isComplete = true;
  for(var i = 0;i < inputsArr.length;i++) {
    var nameParts = inputsArr[i].split('_');
    if(nameParts[0] != 'txtarea'){
      if(nameParts[0] == 'img') {
        if(!document.getElementsByName("compID_" + nameParts[1])[0] && (document.getElementsByName(inputsArr[i])[0].value == '' || document.getElementsByName(inputsArr[i])[0].value == null)) {
          alert(inputsArr[i]);
          isComplete = false;
        }
      }else{
        if(document.getElementsByName(inputsArr[i])[0].value == '' || document.getElementsByName(inputsArr[i])[0].value == null) {
          alert(inputsArr[i]);
          isComplete = false;
        }
      }
    }
  }
  if(isComplete){
    //document.getElementById("pls-wait").innerHTML = "Loading..."
    tinymce.triggerSave();
    document.getElementById("blog-form").submit();
  }else{
    alert('Please fill-out everything');
  }
}

function removeInput(index,inputID) {
  var container = document.getElementById("input-container" + index);
  var compSeqInput = document.getElementById("compSeq");
  container.innerHTML = "";
  var arr = compSeqInput.value.split(',');
  var newStr = '';
  for(var i = 0;i < arr.length;i++) {
    if(arr[i] != inputID)
    {
        if(newStr == '') {
          newStr += arr[i];
        }else{
          newStr += ',' + arr[i];
        }
    }
  }
  compSeqInput.value = newStr;
  appendToDelImgInEdit(inputID);
}
function appendToDelImgInEdit(inputID) {
  var type = inputID.split('_');
  var toDel = document.getElementById("toDelImg");
  if(type[0] == 'img') {
    if(toDel) {
      if(toDel.value == '') {
        toDel.value += inputID;
      }else{
        toDel.value += ',' + inputID;
      }
    }
  }
}






function onChangeInputs() {
  var valStr = '';
  var valSeq = document.getElementById('valSeq');
  var compSeq = document.getElementById("compSeq").value;
  compSeqArr = compSeq.split(',');

  for(var i = 0;i < compSeqArr.length;i++) {
    if(valStr == '') {
      valStr += getInputVal(compSeqArr[i]);
    }else{
      valStr += '|' + getInputVal(compSeqArr[i]);
    }
  }
  valSeq.value = valStr;
}
function getInputVal(comp) {
var prefix = comp.split('_');
var retVal = '';
switch(prefix[0]) {
  case 'h1':
      retVal = document.getElementsByName(comp)[0].value;
  break;
  case 'txtarea':
      retVal = (((tinyMCE.get(comp).getContent()).replace(/(&nbsp;)*/g, "")).replace(/(<p>)*/g, "")).replace(/<(\/)?p[^>]*>/g, "");
  break;
  case 'img':
      retVal = '';
  break;
}
return retVal;
}
function addComponent(type) {
  var compSeq = document.getElementById("compSeq");
  var nextIndex = 2;
  if(compSeq.value != '') {
    nextIndex = 3;
  }
  if(compSeq.value.includes(",") == true){
    var arr2 = compSeq.value.split(',');
    nextIndex = arr2.length + 2;
    
  }
  
  if(compSeq.value == '') {
    compSeq.value += type + '_' + nextIndex;
  }else{
    compSeq.value += ',' + type + '_' + nextIndex;
  }
  getBlogComponentV2();
}
function getBlogComponentV2() {
  var xhttp = new XMLHttpRequest();
  var rootUrl = document.getElementById("rootUrl").value;
  var compSeq = document.getElementById("compSeq").value;
  var compSeq2 = compSeq.replace(/,/g, "|");
  var valSeq = document.getElementById("valSeq").value;
  if(valSeq == '') {
    valSeq = '%';
  }
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("comp-container").innerHTML = this.responseText;
     tinymce.init({
      selector: 'textarea.ptxtarea',
      /*init_instance_callback: function (editor) {
        editor.on('keyup', function (e) {
          onChangeInputs();
        });
      }*/
     });
    }
  };
  xhttp.open("GET", rootUrl + "/get-add-component-v2/" + compSeq2 + "/" + valSeq, true);
  xhttp.send();
}